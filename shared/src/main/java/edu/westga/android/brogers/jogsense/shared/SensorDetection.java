package edu.westga.android.brogers.jogsense.shared;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.Arrays;

/**
 * SensorDetection - Helper class to wrap calls relating to Sensor checks.
 *
 * @author Brian Rogers
 * @version October 29th, 2014
 */
public class SensorDetection {
    private static final String TAG = "Shared-SensorDetection";

    //TODO: Refactor project to consolidate all this information.
    private static final int TYPE_ADPD142_LIB2 = 65562;
    //TODO: Add cases for remapped Heart Rate Sensors
    private static final Integer[] HEART_RATE_TYPES = {Sensor.TYPE_HEART_RATE, TYPE_ADPD142_LIB2};
    //TODO: Add cases for remapped Step Counter Sensors
    private static final Integer[] STEP_COUNTER_TYPES = {Sensor.TYPE_STEP_COUNTER};

    /**
     * Checks if a valid Step Counter sensor exists on the device.
     *
     * @param sensorManager Accessor object for the device's {@link Sensor}s.
     * @return true if such a sensor exists, false otherwise.
     */
    public static boolean hasStepCounterSensor(SensorManager sensorManager) {
        for (int sensorType : STEP_COUNTER_TYPES) {
            Sensor stepSensor = sensorManager.getDefaultSensor(sensorType);

            if (stepSensor != null) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if a valid Heart Rate sensor exists on the device.
     *
     * @param sensorManager Accessor object for the device's {@link Sensor}s.
     * @return true if such a sensor exists, false otherwise.
     */
    public static boolean hasHeartRateSensor(SensorManager sensorManager) {
        for (int sensorType : HEART_RATE_TYPES) {
            Sensor heartRateSensor = sensorManager.getDefaultSensor(sensorType);

            if (heartRateSensor != null) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if a given {@link Sensor} is a valid Step Counter sensor.
     *
     * @param sensor The {@link Sensor} to validate.
     * @return true if the {@link Sensor} is a Step Counter sensor, false otherwise.
     */
    public static boolean isStepCounterSensor(Sensor sensor) {
        int sensorType = sensor.getType();
        Log.d(TAG, "Sensor type: " + sensorType);

        boolean result = Arrays.asList(STEP_COUNTER_TYPES).contains(sensorType);
        Log.d(TAG, "isStepCounterSensor: " + result);

        return result;
    }

    /**
     * Checks if a given {@link Sensor} is a valid Heart Rate sensor.
     *
     * @param sensor The {@link Sensor} to validate.
     * @return true if the {@link Sensor} is a Heart Rate sensor, false otherwise.
     */
    public static boolean isHeartRateSensor(Sensor sensor) {
        int sensorType = sensor.getType();
        Log.d(TAG, "Sensor type: " + sensorType);

        boolean result = Arrays.asList(HEART_RATE_TYPES).contains(sensorType);
        Log.d(TAG, "isHeartRateSensor: " + result);

        return result;
    }

    /**
     * Finds the Sensor ID of the step counter sensor that should be used.
     *
     * @param sensorManager Accessor object for the device's {@link Sensor}s.
     * @return The step counter sensor ID that should be used.
     */
    public static int getIdealStepCounterSensor(SensorManager sensorManager) {
        for (int sensorType : STEP_COUNTER_TYPES) {
            Sensor stepSensor = sensorManager.getDefaultSensor(sensorType);

            if (stepSensor != null && sensorType != Sensor.TYPE_STEP_COUNTER) {
                return sensorType;
            }
        }

        return Sensor.TYPE_STEP_COUNTER;
    }

    /**
     * Finds the Sensor ID of the heart rate sensor that should be used.
     *
     * @param sensorManager Accessor object for the device's {@link Sensor}s.
     * @return The heart rate sensor ID that should be used.
     */
    public static int getIdealHeartRateSensor(SensorManager sensorManager) {
        for (int sensorType : HEART_RATE_TYPES) {
            Sensor heartRateSensor = sensorManager.getDefaultSensor(sensorType);

            if (heartRateSensor != null && sensorType != Sensor.TYPE_HEART_RATE) {
                return sensorType;
            }
        }

        return Sensor.TYPE_HEART_RATE;
    }
}
