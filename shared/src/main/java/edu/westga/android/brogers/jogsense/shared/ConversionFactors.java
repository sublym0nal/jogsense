package edu.westga.android.brogers.jogsense.shared;

/**
 * Created by Brian Rogers on 11/22/2014.
 */
public class ConversionFactors {
    //Weight
    public static final double KILOGRAMS_PER_POUND = 0.4536;
    public static final double KILOGRAMS_PER_STONE = 6.35;

    public static final double POUNDS_PER_KILOGRAM = 2.205;
    public static final double POUNDS_PER_STONE = 14;

    public static final double STONES_PER_KILOGRAM = 0.4536;
    public static final double STONES_PER_POUND = 0.07143;

    //Distance
    public static final double METERS_PER_KILOMETER = 1000;
    public static final double METERS_PER_MILE = 1609;

    public static final double KILOMETERS_PER_METER = 0.001;
    public static final double KILOMETERS_PER_MILE = 1.609;

    public static final double MILES_PER_METER = 0.0006214;
    public static final double MILES_PER_KILOMETER = 0.6214;

    //Speed
    public static final double METERS_PER_SECOND_TO_MILES_PER_HOUR = 2.237;
    public static final double MILES_PER_HOUR_TO_METERS_PER_SECOND = 0.44704;

    public static final double METERS_PER_SECOND_TO_KILOMETERS_PER_HOUR = 3.6;
    public static final double KILOMETERS_PER_HOUR_TO_METERS_PER_SECOND = 0.277777778;

    //Energy
    public static final double CALORIES_PER_KILOJOULE = 239;
    public static final double KILOJOULES_PER_CALORIE = 0.004184;
}
