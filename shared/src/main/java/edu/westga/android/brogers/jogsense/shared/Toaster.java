package edu.westga.android.brogers.jogsense.shared;

import android.content.Context;
import android.widget.Toast;

import java.util.Locale;

/**
 * Toaster - Helper class intended to wrap repetitive calls relating to creating {@link Toast}s to help
 * reduce code clutter.
 *
 * @author Brian Rogers
 * @version October 22nd, 2014
 */
@SuppressWarnings("UnusedDeclaration")
public class Toaster {
    /**
     * Shows a {@link Toast} for a short period of time.
     *
     * @param context The context in which to be displayed.
     * @param message The message to be displayed.
     */
    public static void showShortToast(final Context context, final String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Deprecated
    /**
     * Shows a {@link Toast} for a short period of time.
     * @pre The formatted message will assume a US Locale.
     * @param context The context in which to be displayed.
     * @param message The unformatted message to be parsed and displayed.
     * @param formatArguments The arguments with which to format the message.
     * @deprecated Avoid use in production code!
     */
    public static void showShortToast(final Context context, final String message, Object... formatArguments) {
        String formattedMessage = String.format(Locale.US, message, formatArguments);
        Toast.makeText(context, formattedMessage, Toast.LENGTH_SHORT).show();
    }

    /**
     * Shows a {@link Toast} for a short period of time.
     *
     * @param context          The context in which to be displayed.
     * @param messageReference The reference to the message to be displayed.
     */
    public static void showShortToast(final Context context, final int messageReference) {
        Toast.makeText(context, messageReference, Toast.LENGTH_SHORT).show();
    }

    /**
     * Shows a {@link Toast} for a short period of time.
     *
     * @param context          The context in which to be displayed.
     * @param messageReference The reference to the unformatted message to be parsed and displayed.
     * @param formatArguments  The arguments with which to format the message.
     */
    public static void showShortToast(final Context context, final int messageReference, Object... formatArguments) {
        String message = context.getString(messageReference, formatArguments);
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Shows a {@link Toast} for a long period of time.
     *
     * @param context The context in which to be displayed.
     * @param message The message to display in the Toast.
     */
    public static void showLongToast(final Context context, final String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    @Deprecated
    /**
     * Shows a {@link Toast} for a long period of time.
     * @pre The formatted message will assume a US Locale.
     * @param context The context in which to be displayed.
     * @param message The unformatted message to be parsed and displayed.
     * @param formatArguments The arguments with which to format the message.
     * @deprecated Avoid use in production code!
     */
    public static void showLongToast(final Context context, final String message, Object... formatArguments) {
        String formattedMessage = String.format(Locale.US, message, formatArguments);
        Toast.makeText(context, formattedMessage, Toast.LENGTH_LONG).show();
    }

    /**
     * Shows a {@link Toast} for a long period of time.
     *
     * @param context          The context in which to be displayed.
     * @param messageReference The reference to the message to be displayed.
     */
    public static void showLongToast(final Context context, final int messageReference) {
        Toast.makeText(context, messageReference, Toast.LENGTH_LONG).show();
    }

    /**
     * Shows a {@link Toast} for a long period of time.
     *
     * @param context          The context in which to be displayed.
     * @param messageReference The reference to the unformatted message to be parsed and displayed.
     * @param formatArguments  The arguments with which to format the message.
     */
    public static void showLongToast(final Context context, final int messageReference, Object... formatArguments) {
        String message = context.getString(messageReference, formatArguments);
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
