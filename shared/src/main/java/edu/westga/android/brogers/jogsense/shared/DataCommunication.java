package edu.westga.android.brogers.jogsense.shared;

import android.content.Context;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

/**
 * DataCommunication - Helper class to wrap calls relating to sending data between the handheld and
 * Wear applications.
 *
 * @author Brian Rogers
 * @version October 29th, 2014
 */
public class DataCommunication {
    public static final long CONNECTION_TIMEOUT_IN_MILLISECONDS = 100;
    private static final String TAG = "Shared-DataCommunication";

    /**
     * Builds a {@link GoogleApiClient} with access to the Wearable API.
     *
     * @param context The context to use for the connection.
     * @return A {@link GoogleApiClient} with access to the Wearable API.
     */
    public static GoogleApiClient buildGoogleWearableApiClient(Context context) {
        return new GoogleApiClient.Builder(context).addApi(Wearable.API).build();
    }

    /**
     * Types of Messages to be sent between the handheld and Wear applications.
     */
    public enum MessageType {
        /**
         * Data obtained from the Step Counter Sensor.
         */
        STEP_COUNTER("StepCounter"),

        /**
         * Data obtained from the Heart Rate Sensor.
         */
        HEART_RATE("HeartRate"),

        /**
         * Signal to start a new Session.
         */
        SESSION_START("SessionStart"),

        /**
         * Signal to open Session Review on the handheld device.
         */
        REVIEW_SESSIONS("ReviewSessions"),

        /**
         * Signal to open Settings on the handheld device.
         */
        CONFIGURE_SETTINGS("ConfigureSettings");

        private final String text;

        private MessageType(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }
}
