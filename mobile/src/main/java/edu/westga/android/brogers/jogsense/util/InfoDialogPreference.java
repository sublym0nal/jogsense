package edu.westga.android.brogers.jogsense.util;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;

/**
 * InfoDialogPreference - Blank implementation of DialogPreference to facilitate
 * use as a generic dialog box.
 *
 * @author Brian Rogers
 * @version October 14th, 2014
 */
public class InfoDialogPreference extends DialogPreference {
    public InfoDialogPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}