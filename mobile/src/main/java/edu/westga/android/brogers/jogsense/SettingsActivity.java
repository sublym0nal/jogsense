package edu.westga.android.brogers.jogsense;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;

import org.bostonandroid.datepreference.DatePreference;

import java.util.List;

import edu.westga.android.brogers.jogsense.util.HardwareDetection;
import edu.westga.android.brogers.jogsense.util.PreferencesHelper;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
@SuppressWarnings("deprecation")
public class SettingsActivity extends PreferenceActivity {
    private static final String TAG = "Mobile-SettingsActivity";
    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);

            } else if (preference instanceof RingtonePreference) {
                // For ringtone preferences, look up the correct display value
                // using RingtoneManager.
                if (TextUtils.isEmpty(stringValue)) {
                    // Empty values correspond to 'silent' (no ringtone).
                    //preference.setSummary(R.string.pref_ringtone_silent);

                } else {
                    Ringtone ringtone = RingtoneManager.getRingtone(
                            preference.getContext(), Uri.parse(stringValue));

                    if (ringtone == null) {
                        // Clear the summary if there was a lookup error.
                        preference.setSummary(null);
                    } else {
                        // Set the summary to reflect the new ringtone display
                        // name.
                        String name = ringtone.getTitle(preference.getContext());
                        preference.setSummary(name);
                    }
                }
            } else if (preference instanceof DatePreference) {
                DatePreference dp = (DatePreference) preference;
                dp.persistDate(stringValue);
            } else if (preference.getKey().equals(preference.getContext().getString(R.string.weight_key))) {
                Context context = preference.getContext();
                PreferencesHelper prefHelper = new PreferencesHelper(context);
                String weightUnit = prefHelper.getString(R.string.weight_unit_key, R.string.pref_unit_kilograms);
                String unitAbbreviation = "";

                if (weightUnit.equals(context.getString(R.string.pref_unit_pounds))) {
                    int quantity = 0;

                    try {
                        double number = Double.parseDouble(stringValue);
                        quantity = (int) number;
                    } catch (NumberFormatException ex) {
                        Log.e(TAG, "Improper number", ex);
                    }

                    unitAbbreviation = context.getResources().getQuantityString(R.plurals.abbreviation_pounds, quantity);
                } else if (weightUnit.equals(context.getString(R.string.pref_unit_kilograms))) {
                    unitAbbreviation = context.getString(R.string.abbreviation_kilograms);
                } else if (weightUnit.equals(context.getString(R.string.pref_unit_stones))) {
                    unitAbbreviation = context.getString(R.string.abbreviation_stones);
                }

                if (stringValue.isEmpty()) {
                    preference.setSummary("0 " + unitAbbreviation);
                } else {
                    preference.setSummary(stringValue + " " + unitAbbreviation);
                }
            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);
            }
            return true;
        }
    };
    private static final String HARDWARE_MONITORS_FRAGMENT_NAME = "HardwareMonitors";
    private static final String UNITS_FRAGMENT_NAME = "Units";
    private static final String IS_WITHIN_SUBMENU_KEY = "isWithinSubmenu";
    private static final String SUBMENU_FRAGMENT_ID_KEY = "submenuFragmentId";
    private static final String SUBMENU_NAME_KEY = "submenuName";
    /**
     * Determines whether to always show the simplified settings UI, where
     * settings are presented in a single list. When false, settings are shown
     * as a master/detail two-pane view on tablets. When true, a single pane is
     * shown on tablets.
     */
    private static final boolean ALWAYS_SIMPLE_PREFS = false;
    private boolean isWithinSubmenu;
    private int submenuFragmentId = 0;
    private String submenuName;

    /**
     * Helper method to determine if the device has a large screen.
     */
    private static boolean hasLargeScreen(Context context) {
        return is7InchTablet(context);
    }

    private static boolean is7InchTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    /**
     * Determines whether the simplified settings UI should be shown. This is
     * true if this is forced via {@link #ALWAYS_SIMPLE_PREFS}, or the device
     * doesn't have newer APIs like {@link PreferenceFragment}, or the device
     * doesn't have an extra-large screen. In these cases, a single-pane
     * "simplified" settings UI should be shown.
     */
    private static boolean isSimplePreferences(Context context) {
        return ALWAYS_SIMPLE_PREFS
                || Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB
                || !hasLargeScreen(context);
    }

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putBoolean(IS_WITHIN_SUBMENU_KEY, isWithinSubmenu);

        if (submenuFragmentId != 0) {
            outState.putInt(SUBMENU_FRAGMENT_ID_KEY, submenuFragmentId);
            outState.putString(SUBMENU_NAME_KEY, submenuName);
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkHardwareAvailability();
    }

    private void checkStepCounterAvailability() {
        String preferenceKey = getString(R.string.step_counter_sensor_key);
        Preference stepCounterPreference = findPreference(preferenceKey);

//        if (stepCounterPreference != null) {
//            boolean shouldBeEnabled = HardwareDetection.hasStepCounter(this);
//            stepCounterPreference.setEnabled(shouldBeEnabled);
//        }
    }

    private void checkGpsAvailability() {
        String preferenceKey = getString(R.string.gps_key);
        Preference gpsPreference = findPreference(preferenceKey);

        if (gpsPreference != null) {
            boolean shouldBeEnabled = !HardwareDetection.isLocationServiceDisabled(this);
            gpsPreference.setEnabled(shouldBeEnabled);
        }
    }

    private void checkHeartRateSensorAvailability() {
        String preferenceKey = getString(R.string.heart_rate_sensor_key);
        Preference heartRateSensorPreference = findPreference(preferenceKey);

//        if (heartRateSensorPreference != null) {
//            boolean shouldBeEnabled = SensorDetection.hasHeartRateSensor(this);
//            heartRateSensorPreference.setEnabled(shouldBeEnabled);
//        }
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Show the Up button in the action bar.
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            // TODO: If Settings has multiple levels, Up should navigate up
            // that hierarchy.
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        if (savedInstanceState != null) {
            isWithinSubmenu = savedInstanceState.getBoolean(IS_WITHIN_SUBMENU_KEY);
        }

        setupSimplePreferencesScreen();

        if (isWithinSubmenu) {
            if (!onIsMultiPane()) {
                buildHardwareMonitorsSubmenu();
                bindBackButtonToView();
            } else {
                try {
                    if (savedInstanceState != null) {
                        submenuFragmentId = savedInstanceState.getInt(SUBMENU_FRAGMENT_ID_KEY);
                        submenuName = savedInstanceState.getString(SUBMENU_NAME_KEY);

                        if (submenuName != null) {
                            Fragment submenuFragment = getFragmentManager().findFragmentById(submenuFragmentId);
                            bindBackButtonToFragment(submenuFragment);
                        }
                    }
                } catch (NullPointerException ex) {
                    Log.wtf(TAG, ex);
                }
            }
        } else {
            if (!onIsMultiPane()) {
                buildPreferenceScreen();
                linkUnitsSubmenu(false);
                linkHardwareMonitorSubmenu(false);
            }
        }
    }

    private void linkUnitsSubmenu(boolean keyRemapped) {
        Preference unitsPreference = findPreference(getString(R.string.units_key));

        if (unitsPreference != null) {
            unitsPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    getPreferenceScreen().removeAll();
                    buildUnitsSubmenu();
                    isWithinSubmenu = true;

                    bindBackButtonToView();

                    return true;
                }
            });
        }

        if (keyRemapped) {
            getListView().setOnKeyListener(null);
        }
    }

    private void linkHardwareMonitorSubmenu(boolean keyRemapped) {
        Preference hardwareMonitors = findPreference(getString(R.string.hardware_monitors_key));

        if (hardwareMonitors != null) {
            hardwareMonitors.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    getPreferenceScreen().removeAll();
                    buildHardwareMonitorsSubmenu();
                    isWithinSubmenu = true;

                    bindBackButtonToView();

                    return true;
                }
            });
        }

        if (keyRemapped) {
            getListView().setOnKeyListener(null);
        }
    }

    private void buildHardwareMonitorsSubmenu() {
        addPreferenceSection(R.string.pref_title_hardware_monitors, R.xml.pref_hardware_monitors);
        checkHardwareAvailability();
    }

    private void buildUnitsSubmenu() {
        addPreferenceSection(R.string.pref_title_units, R.xml.pref_units);

        bindPreferenceSummaryToValue(findPreference(getString(R.string.weight_unit_key)));
        bindPreferenceSummaryToValue(findPreference(getString(R.string.distance_unit_key)));
        bindPreferenceSummaryToValue(findPreference(getString(R.string.energy_unit_key)));
    }

    private void checkHardwareAvailability() {
        checkStepCounterAvailability();
        checkGpsAvailability();
        checkHeartRateSensorAvailability();
    }

    private void bindBackButtonToView() {
        getListView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    getPreferenceScreen().removeAll();
                    buildPreferenceScreen();
                    linkUnitsSubmenu(true);
                    linkHardwareMonitorSubmenu(true);
                    isWithinSubmenu = false;

                    return true;
                }

                return false;
            }
        });
    }

    private void bindBackButtonToFragment(final Fragment submenuFragment) {
        getListView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    //TODO: Add case for future submenu fragments here
                    if (submenuName.equals(HARDWARE_MONITORS_FRAGMENT_NAME)) {
                        Fragment sessionFragment = new SessionFragment();
                        getFragmentManager().beginTransaction().replace(
                                submenuFragment.getId(), sessionFragment).commit();
                    } else if (submenuName.equals(UNITS_FRAGMENT_NAME)) {
                        Fragment userProfileFragment = new UserProfileFragment();
                        getFragmentManager().beginTransaction().replace(
                                submenuFragment.getId(), userProfileFragment).commit();
                    }

                    getListView().setOnKeyListener(null);
                    isWithinSubmenu = false;
                    submenuFragmentId = 0;
                    submenuName = null;

                    return true;
                }

                return false;
            }
        });
    }

    /**
     * Shows the simplified settings UI if the device configuration if the
     * device configuration dictates that a simplified, single-pane UI should be
     * shown.
     */
    private void setupSimplePreferencesScreen() {
        if (!isSimplePreferences(this)) {
            return;
        }

        // In the simplified UI, fragments are not used at all and we instead
        // use the older PreferenceActivity APIs.
        initializePreferenceScreen();
    }

    private void buildPreferenceScreen() {
        addPreferenceSection(R.string.pref_header_user_profile, R.xml.pref_user_profile);
        bindUserProfilePreferenceValues();

        addPreferenceSection(R.string.pref_header_session, R.xml.pref_session);

        addPreferenceSection(R.string.pref_header_about, R.xml.pref_about);
        addVersionNumberToAboutPreference(this);
        linkGooglePlayPreference(this);
        linkSendFeedbackPreference(this);
    }

    protected void bindUserProfilePreferenceValues() {
        bindPreferenceSummaryToValue(findPreference(getString(R.string.full_name_key)));
        bindPreferenceSummaryToValue(findPreference(getString(R.string.gender_key)));
        bindPreferenceSummaryToValue(findPreference(getString(R.string.birthday_key)));
        bindPreferenceSummaryToValue(findPreference(getString(R.string.weight_key)));
        bindWeightPreferenceDialogTitle();
    }

    private void bindWeightPreferenceDialogTitle() {
        final EditTextPreference weightPreference = (EditTextPreference) findPreference(getString(R.string.weight_key));
        PreferencesHelper prefHelper = new PreferencesHelper(this);
        String unitPreference = prefHelper.getString(R.string.weight_unit_key, R.string.pref_unit_kilometers);

        if (unitPreference.equals(getString(R.string.pref_unit_pounds))) {
            String poundsAbbreviation = getResources().getQuantityString(R.plurals.abbreviation_pounds, 2);
            String dialogTitle = getString(R.string.weight_dialog_title, poundsAbbreviation);
            weightPreference.setDialogTitle(dialogTitle);
        } else if (unitPreference.equals(getString(R.string.pref_unit_kilograms))) {
            String dialogTitle = getString(R.string.weight_dialog_title, getString(R.string.abbreviation_kilograms));
            weightPreference.setDialogTitle(dialogTitle);
        } else if (unitPreference.equals(getString(R.string.pref_unit_stones))) {
            String dialogTitle = getString(R.string.weight_dialog_title, getString(R.string.abbreviation_stones));
            weightPreference.setDialogTitle(dialogTitle);
        }
    }

    private void initializePreferenceScreen() {
        addPreferencesFromResource(R.xml.pref_hidden);

        hidePreference(R.string.is_new_user_key);
        hidePreference(R.string.step_counter_warning_key);
        hidePreference(R.string.location_services_warning_key);
        hidePreference(R.string.location_sources_info_key);
        hidePreference(R.string.heart_rate_sensor_warning_key);
    }

    private void hidePreference(final int preferenceKeyReference) {
        final String preferenceKey = getString(preferenceKeyReference);
        Preference preference = findPreference(preferenceKey);
        getPreferenceScreen().removePreference(preference);
    }

    private void addPreferenceSection(int headerTitleReference, int preferenceScreenReference) {
        PreferenceCategory header = new PreferenceCategory(this);
        String headerTitle = getString(headerTitleReference);

        header.setTitle(headerTitle);
        getPreferenceScreen().addPreference(header);
        addPreferencesFromResource(preferenceScreenReference);
    }

    private void addVersionNumberToAboutPreference(final Context context) {
        String aboutPreferenceKey = context.getString(R.string.about_key);
        Preference aboutPreference = findPreference(aboutPreferenceKey);

        if (aboutPreference != null) {
            try {
                String versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                aboutPreference.setSummary(versionName);
            } catch (PackageManager.NameNotFoundException ex) {
                Log.wtf(TAG, ex);
            }
        }
    }

    private void linkGooglePlayPreference(final Context context) {
        String googlePlayListingKey = context.getString(R.string.google_play_listing_key);
        Preference googlePlayPreference = findPreference(googlePlayListingKey);

        if (googlePlayPreference != null) {
            googlePlayPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    final String packageName = context.getPackageName();
                    String googlePlayPrefix;

                    try {
                        googlePlayPrefix = context.getString(R.string.google_play_listing_prefix);

                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(googlePlayPrefix + packageName));

                        startActivity(intent);
                    } catch (android.content.ActivityNotFoundException ex) {
                        googlePlayPrefix = context.getString(R.string.google_play_web_listing_prefix);

                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(googlePlayPrefix + packageName));

                        startActivity(intent);
                    }

                    return true;
                }
            });
        }
    }

    private void linkSendFeedbackPreference(final Context context) {
        String sendFeedbackKey = context.getString(R.string.send_feedback_key);
        Preference sendFeedbackPreference = findPreference(sendFeedbackKey);

        if (sendFeedbackPreference != null) {
            try {
                String versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                String emailAddress = getString(R.string.feedback_email);
                String emailSubject = getString(R.string.feedback_subject, versionName);

                Intent sendEmailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", emailAddress, null));
                sendEmailIntent.putExtra(Intent.EXTRA_SUBJECT, emailSubject);

                sendFeedbackPreference.setIntent(sendEmailIntent);
            } catch (PackageManager.NameNotFoundException ex) {
                Log.wtf(TAG, ex);
            }
        }
    }

    @Override
    protected boolean isValidFragment(String fragmentName) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return hasLargeScreen(this) && !isSimplePreferences(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target) {
        if (!isSimplePreferences(this)) {
            loadHeadersFromResource(R.xml.pref_headers, target);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class UserProfileFragment extends PreferenceFragment {
        private SettingsActivity parentActivity;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_user_profile);

            parentActivity = (SettingsActivity) getActivity();

            bindPreferenceSummaryToValue(findPreference(getString(R.string.full_name_key)));
            bindPreferenceSummaryToValue(findPreference(getString(R.string.gender_key)));
            bindPreferenceSummaryToValue(findPreference(getString(R.string.birthday_key)));
            bindPreferenceSummaryToValue(findPreference(getString(R.string.weight_key)));
            parentActivity.bindWeightPreferenceDialogTitle();

            final Preference unitsPreference = findPreference(getString(R.string.units_key));
            unitsPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    final Fragment unitsFragment = new UnitsFragment();
                    final SettingsActivity parentActivity = (SettingsActivity) getActivity();

                    getFragmentManager().beginTransaction().replace(UserProfileFragment.this.getId(), unitsFragment).commit();
                    parentActivity.bindBackButtonToFragment(unitsFragment);
                    parentActivity.isWithinSubmenu = true;
                    parentActivity.submenuFragmentId = unitsFragment.getId();
                    parentActivity.submenuName = UNITS_FRAGMENT_NAME;

                    return true;
                }
            });
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class UnitsFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_units);
        }

        @Override
        public void onStop() {
            // Ensures Back button is rebound to proper behavior if the Fragment is dismissed.
            ((SettingsActivity) getActivity()).getListView().setOnKeyListener(null);

            super.onStop();
        }

        @Override
        public void onResume() {
            super.onResume();

            ((SettingsActivity) getActivity()).bindBackButtonToFragment(this);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class SessionFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_session);

            final Preference hardwareMonitorsPreference = findPreference(getString(R.string.hardware_monitors_key));
            hardwareMonitorsPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    final Fragment hardwareMonitorsFragment = new HardwareMonitorsFragment();
                    final SettingsActivity parentActivity = (SettingsActivity) getActivity();

                    getFragmentManager().beginTransaction().replace(SessionFragment.this.getId(), hardwareMonitorsFragment).commit();
                    parentActivity.bindBackButtonToFragment(hardwareMonitorsFragment);
                    parentActivity.isWithinSubmenu = true;
                    parentActivity.submenuFragmentId = hardwareMonitorsFragment.getId();
                    parentActivity.submenuName = HARDWARE_MONITORS_FRAGMENT_NAME;

                    return true;
                }
            });
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class HardwareMonitorsFragment extends PreferenceFragment {
        private SettingsActivity parentActivity;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_hardware_monitors);

            parentActivity = (SettingsActivity) getActivity();
        }

        @Override
        public void onResume() {
            super.onResume();

            parentActivity.checkHardwareAvailability();
            ((SettingsActivity) getActivity()).bindBackButtonToFragment(this);
        }

        @Override
        public void onStop() {
            // Ensures Back button is rebound to proper behavior if the Fragment is dismissed.
            ((SettingsActivity) getActivity()).getListView().setOnKeyListener(null);

            super.onStop();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class AboutFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_about);

            SettingsActivity parentActivity = (SettingsActivity) getActivity();
            parentActivity.linkSendFeedbackPreference(parentActivity);
            parentActivity.addVersionNumberToAboutPreference(parentActivity);
            parentActivity.linkGooglePlayPreference(parentActivity);
        }
    }
}
