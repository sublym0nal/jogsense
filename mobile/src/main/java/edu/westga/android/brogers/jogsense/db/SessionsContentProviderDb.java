package edu.westga.android.brogers.jogsense.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.HashSet;

import static edu.westga.android.brogers.jogsense.db.SessionsContract.Sessions;

/**
 * Created by Brian Rogers on 11/4/2014.
 */
public class SessionsContentProviderDb extends ContentProvider {
    private static final int ALL_SESSIONS = 1;
    private static final int SESSION_ID = 2;

    private static final String AUTHORITY = "edu.westga.android.brogers.sessionsdbprovider";
    private static final String BASE_PATH = "sessions";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH);

    private static final UriMatcher sURIMatcher = new UriMatcher(
            UriMatcher.NO_MATCH);

    static {
        sURIMatcher.addURI(AUTHORITY, BASE_PATH, ALL_SESSIONS);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", SESSION_ID);
    }

    private SessionsDbHelper dbHelper;

    @Override
    /**
     * Initializes the database helper class.
     */
    public boolean onCreate() {
        this.dbHelper = new SessionsDbHelper(getContext());
        return true;
    }

    @Override
    /**
     * Queries the database for specified criteria.
     */
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        this.checkColumns(projection);
        queryBuilder.setTables(Sessions.TABLE_NAME);

        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case ALL_SESSIONS:
                break;
            case SESSION_ID:
                queryBuilder.appendWhere(Sessions.ID + "=" + uri.getLastPathSegment());

                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = this.dbHelper.getReadableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);

        // Notify potential listeners
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    /**
     * Attempts to insert values into the database.
     */
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = this.dbHelper.getWritableDatabase();

        long id = 0;
        switch (uriType) {
            case ALL_SESSIONS:
                id = sqlDB.insert(Sessions.TABLE_NAME,
                        null, values);

                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return Uri.parse(BASE_PATH + "/" + id);
    }

    @Override
    /**
     * Attempts to update values in the database.
     */
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = this.dbHelper.getWritableDatabase();

        int rowsUpdated = 0;
        switch (uriType) {
            case ALL_SESSIONS:
                rowsUpdated = sqlDB.update(Sessions.TABLE_NAME, values, selection, selectionArgs);

                break;
            case SESSION_ID:
                String id = uri.getLastPathSegment();

                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(
                            Sessions.TABLE_NAME, values, Sessions.ID + "=" + id, null);
                } else {
                    rowsUpdated = sqlDB.update(
                            Sessions.TABLE_NAME, values, Sessions.ID + "=" + id + " AND " + selection,
                            selectionArgs);
                }

                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return rowsUpdated;
    }

    @Override
    /**
     * Attempts to delete values in the database.
     */
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = this.dbHelper.getWritableDatabase();

        int rowsDeleted = 0;
        switch (uriType) {
            case ALL_SESSIONS:
                rowsDeleted = sqlDB.delete(Sessions.TABLE_NAME, selection, selectionArgs);

                break;
            case SESSION_ID:
                String id = uri.getLastPathSegment();

                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(
                            Sessions.TABLE_NAME, Sessions.ID + "=" + id, null);
                } else {
                    rowsDeleted = sqlDB.delete(
                            Sessions.TABLE_NAME, Sessions.ID + "=" + id + " AND " + selection,
                            selectionArgs);
                }

                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return rowsDeleted;
    }

    private void checkColumns(String[] projection) {
        if (projection != null) {
            HashSet<String> requestedColumns = new HashSet<String>(
                    Arrays.asList(projection));
            HashSet<String> availableColumns = new HashSet<String>(
                    Arrays.asList(Sessions.ALL_COLUMNS));

            checkIfColumnsInProjection(requestedColumns, availableColumns);
        }
    }

    private void checkIfColumnsInProjection(HashSet<String> requestedColumns,
                                            HashSet<String> availableColumns) {
        if (!availableColumns.containsAll(requestedColumns)) {
            throw new IllegalArgumentException("Unknown columns in projection");
        }
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }
}
