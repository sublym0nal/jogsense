package edu.westga.android.brogers.jogsense;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Locale;

import edu.westga.android.brogers.jogsense.util.HardwareDetection;
import edu.westga.android.brogers.jogsense.util.PreferencesHelper;


public class MainActivity extends FragmentActivity {
    private static final String TAG = "Mobile-MainActivity";

    private PreferencesHelper prefHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new MainFragment())
                    .commit();
        }

        prefHelper = new PreferencesHelper(this);
        checkForNewUser();
    }

    @Override
    protected void onResume() {
        super.onResume();

        new HardwareDetection(this);
    }

    private void checkForNewUser() {
        String isNewUserKey = getString(R.string.is_new_user_key);
        boolean isNewUser = prefHelper.getBoolean(isNewUserKey, R.bool.default_is_new_user);

        if (isNewUser) {
            prefHelper.setBoolean(isNewUserKey, false);

            //TODO: Access profile information in Android 4.0+
            setDefaultUnits();
        }
    }

    private void setDefaultUnits() {
        Locale currentLocale = Locale.getDefault();

        if (currentLocale.equals(Locale.US)) {
            String weightUnitMeasurement = getString(R.string.pref_unit_pounds);
            String distanceUnitMeasurement = getString(R.string.pref_unit_miles);
            String energyUnitMeasurement = getString(R.string.pref_unit_calories);

            prefHelper.setString(R.string.weight_unit_key, weightUnitMeasurement);
            prefHelper.setString(R.string.distance_unit_key, distanceUnitMeasurement);
            prefHelper.setString(R.string.energy_unit_key, energyUnitMeasurement);
        } else {
            String weightUnitMeasurement = getString(R.string.pref_unit_kilograms);
            String distanceUnitMeasurement = getString(R.string.pref_unit_kilometers);
            String energyUnitMeasurement = getString(R.string.pref_unit_calories);

            prefHelper.setString(R.string.weight_unit_key, weightUnitMeasurement);
            prefHelper.setString(R.string.distance_unit_key, distanceUnitMeasurement);
            prefHelper.setString(R.string.energy_unit_key, energyUnitMeasurement);
        }
    }

    public void startRunning(View view) {
        Intent intent = new Intent(this, SessionActivity.class);
        startActivity(intent);
    }

    public void openReviewSessions(View view) {
        Intent intent = new Intent(this, SessionListActivity.class);
        startActivity(intent);
    }

    public void openSettings(View view) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class MainFragment extends Fragment {

        public MainFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_main, container, false);
        }
    }
}
