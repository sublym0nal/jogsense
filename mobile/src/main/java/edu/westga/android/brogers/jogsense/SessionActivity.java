package edu.westga.android.brogers.jogsense;

import android.app.Activity;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import edu.westga.android.brogers.jogsense.db.SessionsDbAdapter;
import edu.westga.android.brogers.jogsense.shared.ConversionFactors;
import edu.westga.android.brogers.jogsense.shared.Toaster;
import edu.westga.android.brogers.jogsense.util.PreferencesHelper;

import static edu.westga.android.brogers.jogsense.shared.DataCommunication.MessageType;

public class SessionActivity extends Activity implements GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener, LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, MessageApi.MessageListener, NodeApi.NodeListener {
    private static final String TAG = "Mobile-SessionActivity";

    private static final int REFRESH_RATE_IN_MILLISECONDS = 10;
    private static final int MILLISECONDS_PER_SECOND = 1000;
    private static final int AMOUNT_PER_UNIT = 60;

    private static final double CALORIES_BURNT_PER_MILE = 0.72;

    private static final LocationRequest REQUEST_PARAMETERS = LocationRequest.create()
            .setInterval(3000)  // Updates, at minimum, once every 3 seconds
            .setFastestInterval((1 / 60l) * 1000)    // Updates, at maximum, at a rate corresponding to 60 FPS (~16.667 ms)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    // SavedInstanceState keys
    private static final String COORDINATES_KEY = "coordinates";
    private static final String STEPS_TAKEN_KEY = "stepsTaken";
    private static final String ENERGY_BURNT_KEY = "energyBurnt";
    private static final String DISTANCE_KEY = "distance";
    private static final String SPEEDS_KEY = "speeds";
    private static final String HEART_RATES_KEY = "heartRates";
    private static final String IS_RUNNING_KEY = "isRunning";
    private static final String START_TIME_KEY = "startTime";
    private static final String ELAPSED_TIME_KEY = "elapsedTime";
    private static final String WAS_LOCATION_CLIENT_CONNECTED_KEY = "wasLocationClientConnected";

    private static final String ACTION_STATUS_KEY = "actionStatus";
    private static final String START_COMMAND_VALUE = "ActiveActionStatus";
    private static final String STOP_COMMAND_VALUE = "CompletedActionStatus";
    private GoogleApiClient googleApiClient;
    private LocationClient locationClient;
    private Location previousLocation;
    private PreferencesHelper prefHelper;
    private double userWeight;
    private long startTime;
    private long elapsedTime = 0;
    private Runnable startTimer = new Runnable() {
        public void run() {
            elapsedTime = System.currentTimeMillis() - startTime;
            updateTimer(elapsedTime);
            handler.postDelayed(this, REFRESH_RATE_IN_MILLISECONDS);
        }
    };
    // UI elements
    private Handler handler = new Handler();
    private GoogleMap map;
    private TextView timer;
    private TextView stepsTakenView;
    private TextView currentSpeedView;
    private TextView currentHeartRateView;
    private Button startButton;
    private Button stopButton;

    // Raw values
    private ArrayList<LatLng> coordinates;
    private int numberOfStepsTaken;

    /**
     * Energy burnt (calories).
     */
    private double energyBurnt;

    /**
     * Distance ran (meters).
     */
    private double distance;

    /**
     * Speeds measured (meters per second).
     */
    private ArrayList<Double> speeds;

    /**
     * Heart rates measured (beats per minute).
     */
    private ArrayList<Double> heartRates;

    // Flags
    private boolean isRunning;
    private boolean cancelRequested;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session);

        Bundle extras = getIntent().getExtras();

        prefHelper = new PreferencesHelper(this);
        userWeight = calculateWeightInPounds();

        timer = (TextView) findViewById(R.id.timer);
        stepsTakenView = (TextView) findViewById(R.id.stepsValue);
        currentSpeedView = (TextView) findViewById(R.id.speedValue);
        currentHeartRateView = (TextView) findViewById(R.id.heartRateValue);
        startButton = (Button) findViewById(R.id.startButton);
        stopButton = (Button) findViewById(R.id.stopButton);

        coordinates = new ArrayList<LatLng>();
        speeds = new ArrayList<Double>();
        heartRates = new ArrayList<Double>();

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Wearable.API)
                .build();

        getGoogleMap();

        if (extras != null) {
            String status = extras.getString(ACTION_STATUS_KEY);

            if (status != null) {
                if (status.equals(START_COMMAND_VALUE)) {
                    start(null);
                } else if (status.equals(STOP_COMMAND_VALUE)) {
                    stop(null);
                }
            }
        }

        prefHelper = new PreferencesHelper(this);

        if (prefHelper.getBoolean(R.string.keep_screen_on_key, R.bool.default_keep_screen_on)) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        getGoogleMap();
        getLocationClient();

        locationClient.connect();
    }

    private void getGoogleMap() {
        if (map == null) {
            map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        }

        if (map != null) {
            map.setMyLocationEnabled(true);
            map.getUiSettings().setZoomControlsEnabled(false);
            map.getUiSettings().setZoomGesturesEnabled(false);
        }
    }

    private void getLocationClient() {
        if (locationClient == null) {
            locationClient = new LocationClient(getApplicationContext(),
                    this,   // ConnectionCallbacks
                    this    // OnConnectionFailedListener
            );
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        double[] speedsArray = convertDoubleListToDoubleArray(speeds);
        double[] heartRatesArray = convertDoubleListToDoubleArray(heartRates);

        outState.putParcelableArrayList(COORDINATES_KEY, coordinates);
        outState.putInt(STEPS_TAKEN_KEY, numberOfStepsTaken);
        outState.putDouble(ENERGY_BURNT_KEY, energyBurnt);
        outState.putDouble(DISTANCE_KEY, distance);
        outState.putDoubleArray(SPEEDS_KEY, speedsArray);
        outState.putDoubleArray(HEART_RATES_KEY, heartRatesArray);
        outState.putBoolean(WAS_LOCATION_CLIENT_CONNECTED_KEY, locationClient.isConnected());
        outState.putLong(START_TIME_KEY, startTime);
        outState.putLong(ELAPSED_TIME_KEY, elapsedTime);
        outState.putBoolean(IS_RUNNING_KEY, isRunning);

        super.onSaveInstanceState(outState);
    }

    private double[] convertDoubleListToDoubleArray(List<Double> list) {
        double[] array = new double[list.size()];

        for (int i = 0; i < array.length; i++) {
            array[i] = list.get(i);
        }

        return array;
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        coordinates = savedInstanceState.getParcelableArrayList(COORDINATES_KEY);

        if (coordinates != null && coordinates.size() >= 1) {
            LatLng rawLatLng = coordinates.get(coordinates.size() - 1);
            previousLocation = new Location("");
            previousLocation.setLatitude(rawLatLng.latitude);
            previousLocation.setLongitude(rawLatLng.longitude);
        }

        if (coordinates != null && coordinates.size() > 1) {
            for (int i = 0; i < coordinates.size() - 1; i++) {
                LatLng start = coordinates.get(i);
                LatLng end = coordinates.get(i + 1);

                drawPath(start, end);
            }
        }

        numberOfStepsTaken = savedInstanceState.getInt(STEPS_TAKEN_KEY);
        energyBurnt = savedInstanceState.getDouble(ENERGY_BURNT_KEY);
        distance = savedInstanceState.getDouble(DISTANCE_KEY);

        double[] speedsArray = savedInstanceState.getDoubleArray(SPEEDS_KEY);
        Double[] speedsObjectArray = convertPrimitiveDoubleArrayToDoubleObjectArray(speedsArray);
        List<Double> speedsList = Arrays.asList(speedsObjectArray);
        speeds.addAll(speedsList);

        double[] heartRatesArray = savedInstanceState.getDoubleArray(HEART_RATES_KEY);
        Double[] heartRatesObjectArray = convertPrimitiveDoubleArrayToDoubleObjectArray(heartRatesArray);
        List<Double> heartRatesList = Arrays.asList(heartRatesObjectArray);
        heartRates.addAll(heartRatesList);

        boolean locationClientWasConnected = savedInstanceState.getBoolean(WAS_LOCATION_CLIENT_CONNECTED_KEY);

        if (locationClientWasConnected) {
            locationClient.connect();
        }

        isRunning = savedInstanceState.getBoolean(IS_RUNNING_KEY);

        if (isRunning) {
            stopButton.setEnabled(true);
            startButton.setEnabled(false);
        }

        startTime = savedInstanceState.getLong(START_TIME_KEY);
        elapsedTime = savedInstanceState.getLong(ELAPSED_TIME_KEY);

        updateTimer(elapsedTime);

        if (isRunning) {
            handler.removeCallbacks(startTimer);
            handler.postDelayed(startTimer, 0);
        }
    }

    private Double[] convertPrimitiveDoubleArrayToDoubleObjectArray(double[] primitiveArray) {
        Double[] objectArray = new Double[primitiveArray.length];

        for (int i = 0; i < objectArray.length; i++) {
            objectArray[i] = primitiveArray[i];
        }

        return objectArray;
    }

    @Override
    protected void onStart() {
        super.onStart();

        googleApiClient.connect();
        Log.d(TAG, "GoogleApiClient connection requested.");
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (locationClient != null) {
            locationClient.disconnect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        Wearable.MessageApi.removeListener(googleApiClient, this);
        Wearable.NodeApi.removeListener(googleApiClient, this);
        Log.d(TAG, "Wearable listeners removed.");

        if (locationClient != null) {
            locationClient.disconnect();
        }
    }

    public void start(View view) {
        startTime = System.currentTimeMillis() - elapsedTime;
        isRunning = true;

        startButton.setEnabled(false);
        stopButton.setEnabled(true);

        handler.removeCallbacks(startTimer);
        handler.postDelayed(startTimer, 0);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int keyCode = event.getKeyCode();

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isRunning && !cancelRequested) {
                cancelRequested = true;
                Toaster.showLongToast(this, R.string.confirm_cancel);

                return true;
            }
        }

        return super.dispatchKeyEvent(event);
    }

    public void stop(View view) {
        isRunning = false;
        handler.removeCallbacks(startTimer);

        SessionsDbAdapter adapter = new SessionsDbAdapter(this);
        adapter.open();

        long endingTime = System.currentTimeMillis();
        double avgSpeed = getAverageOfList(speeds);

        double peakSpeed;
        if (!speeds.isEmpty()) {
            peakSpeed = Collections.max(speeds);
        } else {
            peakSpeed = 0;
        }

        double avgHeartRate = getAverageOfList(heartRates);

        double peakHeartRate;
        if (!heartRates.isEmpty()) {
            peakHeartRate = Collections.max(heartRates);
        } else {
            peakHeartRate = 0;
        }

        long id = adapter.insertSession(startTime, endingTime, numberOfStepsTaken, energyBurnt, distance,
                avgSpeed, peakSpeed, avgHeartRate, peakHeartRate);

        adapter.insertCoordinates(id, coordinates);

        adapter.close();

        //TODO: Allow user to immediately review session
        finish();
    }

    private double calculateTotalDistance() {
        double sum = 0;
        float[] buffer = new float[1];

        if (coordinates != null && coordinates.size() > 1) {
            for (int i = 0; i < coordinates.size() - 1; i++) {
                LatLng start = coordinates.get(i);
                LatLng end = coordinates.get(i + 1);

                Location.distanceBetween(start.latitude, start.longitude, end.latitude,
                        end.longitude, buffer);

                sum += buffer[0];
            }
        }

        return sum;
    }

    private double calculateEnergyBurntInCalories() {
        double distanceInMiles = distance * ConversionFactors.MILES_PER_METER;

        return CALORIES_BURNT_PER_MILE * distanceInMiles * userWeight;
    }

    private double calculateWeightInPounds() {
        String weightValue = prefHelper.getString(R.string.weight_key, R.string.default_weight);
        double rawWeightValue;

        try {
            rawWeightValue = Double.parseDouble(weightValue);
        } catch (NumberFormatException ex) {
            return 0;
        }

        String weightUnit = prefHelper.getString(R.string.weight_unit_key, R.string.pref_unit_kilograms);

        if (weightUnit.equals(getString(R.string.pref_unit_kilograms))) {
            rawWeightValue *= ConversionFactors.POUNDS_PER_KILOGRAM;
        } else if (weightUnit.equals(getString(R.string.pref_unit_stones))) {
            rawWeightValue *= ConversionFactors.POUNDS_PER_STONE;
        }

        return rawWeightValue;
    }

    private double getAverageOfList(List<Double> values) {
        double sum = 0;

        if (!values.isEmpty()) {
            for (double value : values) {
                sum += value;
            }

            return (sum / values.size());
        }

        return sum;
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected");

        Wearable.MessageApi.addListener(googleApiClient, this);
        Wearable.NodeApi.addListener(googleApiClient, this);
        Log.d(TAG, "Wearable listeners added.");

        if (locationClient.isConnected()) {
            locationClient.requestLocationUpdates(REQUEST_PARAMETERS, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed");
    }

    @Override
    public void onPeerConnected(Node node) {
        Toaster.showLongToast(this, "Node " + node.getId() + " connected.");
    }

    @Override
    public void onPeerDisconnected(Node node) {
        Toaster.showLongToast(this, "Node " + node.getId() + " disconnected.");
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        String messageType = messageEvent.getPath();
        byte[] data = messageEvent.getData();

        Log.d(TAG, "Message received.");

        if (messageType.equals(MessageType.STEP_COUNTER.toString())) {
            boolean isEnabled = prefHelper.getBoolean(R.string.step_counter_sensor_key, R.bool.default_step_counter_sensor);

            if (isEnabled) {
                ByteBuffer buffer = ByteBuffer.wrap(data);
                this.numberOfStepsTaken = buffer.getInt();

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        stepsTakenView.setText(Integer.toString(numberOfStepsTaken));
                    }
                });
            }
        } else if (messageType.equals(MessageType.HEART_RATE.toString())) {
            boolean isEnabled = prefHelper.getBoolean(R.string.heart_rate_sensor_key, R.bool.default_heart_rate_sensor);

            if (isEnabled) {
                ByteBuffer buffer = ByteBuffer.wrap(data);
                final int heartRate = buffer.getInt();
                this.heartRates.add((double) heartRate);

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        String bpmCount = getResources().getString(R.string.bpm_count, heartRate);
                        currentHeartRateView.setText(bpmCount);
                    }
                });
            }
        }
    }

    private void updateTimer(float time) {
        long secondsValue = (long) (time / MILLISECONDS_PER_SECOND);
        long minutesValue = (long) ((time / MILLISECONDS_PER_SECOND) / AMOUNT_PER_UNIT);

        String minutesString = formatTimeUnitString(minutesValue);
        String secondsString = formatTimeUnitString(secondsValue);
        String millisecondsString = formatMillisecondsString(time);

        String timeFormat = getString(R.string.time_format);
        String formattedTime = String.format(Locale.US, timeFormat,
                minutesString, secondsString, millisecondsString);

        timer.setText(formattedTime);
    }

    private String formatTimeUnitString(long unitValue) {
        unitValue = unitValue % AMOUNT_PER_UNIT;
        String timeUnitFormat = getString(R.string.time_unit_format);

        return String.format(Locale.US, timeUnitFormat, unitValue);
    }

    private String formatMillisecondsString(float time) {
        String millisecondsString = String.valueOf((long) time);

        if (millisecondsString.length() >= 3) {
            millisecondsString = millisecondsString.substring(
                    millisecondsString.length() - 3,
                    millisecondsString.length() - 1);
        }

        return millisecondsString;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (previousLocation == null) {
            previousLocation = location;
        }

        double startLatitude = previousLocation.getLatitude();
        double startLongitude = previousLocation.getLongitude();
        LatLng startPoint = new LatLng(startLatitude, startLongitude);

        if (coordinates.isEmpty()) {
            coordinates.add(startPoint);
        }

        double endLatitude = location.getLatitude();
        double endLongitude = location.getLongitude();
        LatLng endPoint = new LatLng(endLatitude, endLongitude);

        if (isRunning) {
            coordinates.add(endPoint);
            distance = calculateTotalDistance();
            energyBurnt = calculateEnergyBurntInCalories();
        }

        moveMap(endPoint);

        if (isRunning) {
            drawPath(startPoint, endPoint);
        }

        updateCurrentSpeed(location);
        previousLocation = location;
    }

    private void moveMap(LatLng currentLatLng) {
        CameraPosition.Builder builder = CameraPosition.builder();
        builder.target(currentLatLng);
        builder.zoom(20);
        builder.bearing(0);
        builder.build();

        map.moveCamera(CameraUpdateFactory.newCameraPosition(builder.build()));
    }

    private void drawPath(LatLng startPoint, LatLng endPoint) {
        PolylineOptions line = new PolylineOptions();
        line.add(startPoint, endPoint);
        line.width(10);
        line.color(Color.BLUE);

        map.addPolyline(line);
    }

    private void updateCurrentSpeed(Location location) {
        double rawSpeed = location.getSpeed();
        String unitPreference = prefHelper.getString(R.string.distance_unit_key, R.string.pref_unit_kilometers);

        if (unitPreference.equals(getString(R.string.pref_unit_kilometers))) {
            formatSpeedUnit(rawSpeed, ConversionFactors.METERS_PER_SECOND_TO_KILOMETERS_PER_HOUR,
                    R.string.abbreviation_kilometers_per_hour);
        } else if (unitPreference.equals(getString(R.string.pref_unit_miles))) {
            formatSpeedUnit(rawSpeed, ConversionFactors.METERS_PER_SECOND_TO_MILES_PER_HOUR,
                    R.string.abbreviation_miles_per_hour);
        }

        if (isRunning) {
            speeds.add(rawSpeed);
        }
    }

    private void formatSpeedUnit(final double rawSpeed, final double conversionFactor,
                                 final int unitReference) {
        double convertedSpeed = rawSpeed * conversionFactor;
        String display = getString(R.string.three_decimal_place_unit_template, convertedSpeed,
                getString(unitReference));

        currentSpeedView.setText(display);
    }

    @Override
    public void onDisconnected() {

    }
}
