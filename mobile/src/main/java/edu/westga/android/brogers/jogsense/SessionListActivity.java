package edu.westga.android.brogers.jogsense;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import edu.westga.android.brogers.jogsense.db.SessionsContentProviderDb;
import edu.westga.android.brogers.jogsense.db.SessionsDbHelper;
import edu.westga.android.brogers.jogsense.shared.ConversionFactors;
import edu.westga.android.brogers.jogsense.shared.Toaster;
import edu.westga.android.brogers.jogsense.util.PreferencesHelper;

import static edu.westga.android.brogers.jogsense.db.SessionsContract.Sessions;

public class SessionListActivity extends ListActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String TAG = "mobile-SessionListActivity";

    private static final int MILLISECONDS_PER_SECOND = 1000;
    private static final int AMOUNT_PER_UNIT = 60;

    private MenuItem sortMenuItem;
    private SimpleCursorAdapter adapter;
    private PreferencesHelper prefHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setUpListAdapter();
        prefHelper = new PreferencesHelper(this);
    }

    private void setUpListAdapter() {
        final int[] viewIds = new int[]{0, R.id.timestampIdentifier, 0, 0, R.id.sessionIdentifier,
                0, 0, 0, 0};

        getLoaderManager().initLoader(0, null, this);
        adapter = new SimpleCursorAdapter(this, R.layout.list_item, null, Sessions.ALL_COLUMNS,
                viewIds, 0);
        adapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
            @Override
            public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
                int startTimeDataIndex = Arrays.asList(Sessions.ALL_COLUMNS).indexOf(Sessions.STARTING_TIME);
                long rawStartTime = cursor.getLong(startTimeDataIndex);

                if (view.getId() == R.id.timestampIdentifier) {
                    TextView textView = (TextView) view;
                    DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.LONG);
                    Date date = new Date(rawStartTime);

                    textView.setText(dateFormat.format(date));

                    return true;
                } else if (view.getId() == R.id.sessionIdentifier) {
                    TextView textView = (TextView) view;

                    int endTimeDataIndex = Arrays.asList(Sessions.ALL_COLUMNS).indexOf(Sessions.ENDING_TIME);
                    long rawEndTime = cursor.getLong(endTimeDataIndex);
                    long duration = rawEndTime - rawStartTime;
                    String elapsedTimeString = parseTime(duration);

                    int distanceDataIndex = Arrays.asList(Sessions.ALL_COLUMNS).indexOf(Sessions.DISTANCE);
                    double rawDistance = cursor.getDouble(distanceDataIndex);
                    String distanceString = parseDistance(rawDistance);

                    String display = getString(R.string.session_identifier_template, elapsedTimeString, distanceString);
                    textView.setText(display);

                    return true;
                }

                return false;
            }
        });
        setListAdapter(adapter);

        ListView sessionListView = getListView();
        registerForContextMenu(sessionListView);
    }

    private String parseDistance(double value) {
        String distanceUnit = prefHelper.getString(R.string.distance_unit_key, R.string.pref_unit_kilometers);
        double convertedValue = 0;
        String unit = "";

        if (distanceUnit.equals(getString(R.string.pref_unit_miles))) {
            convertedValue = value * ConversionFactors.MILES_PER_METER;
            unit = getString(R.string.abbreviation_miles);
        } else if (distanceUnit.equals(getString(R.string.pref_unit_kilometers))) {
            convertedValue = value * ConversionFactors.KILOMETERS_PER_METER;
            unit = getString(R.string.abbreviation_kilometers);
        }

        return getString(R.string.three_decimal_place_unit_template, convertedValue, unit);
    }

    private String parseTime(float time) {
        long secondsValue = (long) (time / MILLISECONDS_PER_SECOND);
        long minutesValue = (long) ((time / MILLISECONDS_PER_SECOND) / AMOUNT_PER_UNIT);

        String minutesString = formatTimeUnitString(minutesValue);
        String secondsString = formatTimeUnitString(secondsValue);
        String millisecondsString = formatMillisecondsString(time);

        String timeFormat = getString(R.string.time_format);

        return String.format(Locale.US, timeFormat,
                minutesString, secondsString, millisecondsString);
    }

    private String formatTimeUnitString(long unitValue) {
        unitValue = unitValue % AMOUNT_PER_UNIT;
        String timeUnitFormat = getString(R.string.time_unit_format);

        return String.format(Locale.US, timeUnitFormat, unitValue);
    }

    private String formatMillisecondsString(float time) {
        String millisecondsString = String.valueOf((long) time);

        if (millisecondsString.length() >= 3) {
            millisecondsString = millisecondsString.substring(
                    millisecondsString.length() - 3,
                    millisecondsString.length() - 1);
        }

        return millisecondsString;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_session_list, menu);
        this.sortMenuItem = menu.findItem(R.id.action_sort);

        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        Uri uri = Uri.parse(SessionsContentProviderDb.CONTENT_URI + "/" + info.id);
        Cursor result = getContentResolver().query(uri, Sessions.ALL_COLUMNS,
                Sessions.ID + "=" + uri.getLastPathSegment(), null, null);
        result.moveToFirst();

        String sessionId = result.getString(result.getColumnIndex(Sessions.ID));

        menu.setHeaderTitle(getString(R.string.session_number) + sessionId);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.session_context, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_sort_time_ascending:
                sortAndUpdateMenu(menuItem, R.id.action_sort_time, Sessions.STARTING_TIME, false);
                return true;
            case R.id.action_sort_time_descending:
                sortAndUpdateMenu(menuItem, R.id.action_sort_time, Sessions.STARTING_TIME, true);
                return true;
            case R.id.action_sort_energy_ascending:
                sortAndUpdateMenu(menuItem, R.id.action_sort_energy, Sessions.ENERGY_BURNT, false);
                return true;
            case R.id.action_sort_energy_descending:
                sortAndUpdateMenu(menuItem, R.id.action_sort_energy, Sessions.ENERGY_BURNT, true);
                return true;
            case R.id.action_sort_distance_ascending:
                sortAndUpdateMenu(menuItem, R.id.action_sort_distance, Sessions.DISTANCE, false);
                return true;
            case R.id.action_sort_distance_descending:
                sortAndUpdateMenu(menuItem, R.id.action_sort_distance, Sessions.DISTANCE, true);
                return true;
            case R.id.action_sort_average_speed_ascending:
                sortAndUpdateMenu(menuItem, R.id.action_sort_average_speed, Sessions.AVERAGE_SPEED, false);
                return true;
            case R.id.action_sort_average_speed_descending:
                sortAndUpdateMenu(menuItem, R.id.action_sort_average_speed, Sessions.AVERAGE_SPEED, true);
                return true;
            case R.id.action_sort_peak_speed_ascending:
                sortAndUpdateMenu(menuItem, R.id.action_sort_peak_speed, Sessions.PEAK_SPEED, false);
                return true;
            case R.id.action_sort_peak_speed_descending:
                sortAndUpdateMenu(menuItem, R.id.action_sort_peak_speed, Sessions.PEAK_SPEED, true);
                return true;
            case R.id.action_sort_average_heart_rate_ascending:
                sortAndUpdateMenu(menuItem, R.id.action_sort_average_heart_rate, Sessions.AVERAGE_HEART_RATE, false);
                return true;
            case R.id.action_sort_average_heart_rate_descending:
                sortAndUpdateMenu(menuItem, R.id.action_sort_average_heart_rate, Sessions.AVERAGE_HEART_RATE, true);
                return true;
            case R.id.action_sort_peak_heart_rate_ascending:
                sortAndUpdateMenu(menuItem, R.id.action_sort_peak_heart_rate, Sessions.PEAK_HEART_RATE, false);
                return true;
            case R.id.action_sort_peak_heart_rate_descending:
                sortAndUpdateMenu(menuItem, R.id.action_sort_peak_heart_rate, Sessions.PEAK_HEART_RATE, true);
                return true;
            case R.id.action_delete_all:
                showDeleteAllPrompt();
                return true;
        }

        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
                .getMenuInfo();
        Uri uri = Uri.parse(SessionsContentProviderDb.CONTENT_URI + "/"
                + info.id);
        Cursor result = getContentResolver().query(uri, Sessions.ALL_COLUMNS,
                Sessions.ID + "=" + uri.getLastPathSegment(), null, null);
        result.moveToFirst();

        switch (item.getItemId()) {
            case R.id.viewSession:
                //TODO: Intent to ReviewActivity, shows all data

                return true;
            case R.id.deleteSession:
                getContentResolver().delete(uri, null, null);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void sortAndUpdateMenu(MenuItem orderMenuItem, int dataMenuItemReference, String data,
                                   boolean isDescending) {
        sortData(data, isDescending);

        MenuItem dataMenuItem = this.sortMenuItem.getSubMenu().findItem(dataMenuItemReference);
        dataMenuItem.setChecked(true);
        //TODO: Modify menu to show whether data is being sorted by ascending or descending order

        Toaster.showShortToast(this, R.string.sort_applied, dataMenuItem.getTitle(), orderMenuItem.getTitle());
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, SessionsContentProviderDb.CONTENT_URI,
                Sessions.ALL_COLUMNS, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        this.adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        this.adapter.swapCursor(null);
    }

    private void sortData(String data, boolean descending) {
        String suffix = "";

        if (descending) {
            suffix = SessionsDbHelper.ORDER_BY_DESCENDING;
        }

        Cursor cursor = getContentResolver().query(
                SessionsContentProviderDb.CONTENT_URI, Sessions.ALL_COLUMNS,
                null, null, data + suffix);

        this.adapter.swapCursor(cursor);
    }

    private void showDeleteAllPrompt() {
        if (adapter.getCursor().getCount() == 0) {
            Toaster.showLongToast(this, "Database is already empty!");
        } else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setIcon(android.R.drawable.ic_dialog_alert);
            dialog.setTitle(R.string.dialog_title_delete_all);
            dialog.setMessage(R.string.dialog_message_delete_all);
            dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getContentResolver().delete(SessionsContentProviderDb.CONTENT_URI, null, null);
                }
            });
            dialog.setNegativeButton(android.R.string.no, null);
            dialog.show();
        }
    }
}
