package edu.westga.android.brogers.jogsense.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import static edu.westga.android.brogers.jogsense.db.SessionsContract.Coordinates;
import static edu.westga.android.brogers.jogsense.db.SessionsContract.Sessions;

/**
 * Created by Brian Rogers on 11/4/2014.
 */
public class SessionsDbAdapter {
    private SessionsDbHelper databaseHelper = null;
    private SQLiteDatabase theDb = null;
    private Context context = null;

    /**
     * Creates a new database adapter.
     *
     * @param context The app for which the adapter is being created.
     */
    public SessionsDbAdapter(Context context) {
        this.context = context;
    }

    /**
     * Opens a connection to the database.
     *
     * @return An open connection.
     */
    public SessionsDbAdapter open() {
        this.databaseHelper = new SessionsDbHelper(context);
        this.theDb = this.databaseHelper.getWritableDatabase();

        return this;
    }

    /**
     * Closes the connection to the database.
     */
    public void close() {
        if (databaseHelper.equals(null)) {
            throw new NullPointerException("Database helper is null.");
        } else if (theDb.equals(null)) {
            throw new NullPointerException("Database is null.");
        }

        databaseHelper.close();
        theDb.close();
    }

    public long insertSession(long startingTime, long endingTime, int stepsTaken,
                              double energyBurnt, double distance, double avgSpeed,
                              double peakSpeed, double avgHeartRate, double peakHeartRate) {
        ContentValues values = buildContentValues(startingTime, endingTime, stepsTaken, energyBurnt,
                distance, avgSpeed, peakSpeed, avgHeartRate, peakHeartRate);

        return theDb.insert(Sessions.TABLE_NAME, null, values);
    }

    public boolean insertCoordinates(long sessionId, ArrayList<LatLng> coordinates) {
        long result = -1;
        boolean shouldContinue = true;

        for (int i = 0; shouldContinue; i++) {
            if (i >= coordinates.size()) {
                shouldContinue = false;
            } else {
                ContentValues values = new ContentValues();
                LatLng coordinate = coordinates.get(i);

                values.put(Coordinates.SESSION_ID, sessionId);
                values.put(Coordinates.SEQUENCE_ID, i);
                values.put(Coordinates.LATITUDE, coordinate.latitude);
                values.put(Coordinates.LONGITUDE, coordinate.longitude);

                result = theDb.insert(Coordinates.TABLE_NAME, null, values);

                if (result == -1) {
                    shouldContinue = false;
                }
            }
        }

        return (result != -1);
    }

    public boolean deleteSession(long sessionId) {
        final int result = theDb.delete(Sessions.TABLE_NAME, Sessions.ID + "=" + sessionId,
                null);

        return (result != 0);
    }

    public boolean deleteAllSessions() {
        final int result = theDb.delete(Sessions.TABLE_NAME, "1", null);

        return (result > 0);
    }

    public Cursor getAllSessions() {
        return theDb.query(Sessions.TABLE_NAME, Sessions.ALL_COLUMNS, null, null, null, null, null);
    }

    public Cursor getSession(long sessionId) {
        Cursor cursor = theDb.query(true, Sessions.TABLE_NAME, Sessions.ALL_COLUMNS,
                Sessions.ID + "=" + sessionId, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        return cursor;
    }

    public boolean updateSession(long sessionId, long startingTime, long endingTime, int stepsTaken,
                                 double energyBurnt, double distance, double avgSpeed,
                                 double peakSpeed, double avgHeartRate, double peakHeartRate) {
        ContentValues values = buildContentValues(startingTime, endingTime, stepsTaken, energyBurnt,
                distance, avgSpeed, peakSpeed, avgHeartRate, peakHeartRate);

        return (theDb.update(Sessions.TABLE_NAME, values, Sessions.ID + "=" + sessionId,
                null) > 0);
    }

    private ContentValues buildContentValues(long startingTime, long endingTime, int stepsTaken,
                                             double energyBurnt, double distance, double avgSpeed,
                                             double peakSpeed, double avgHeartRate, double peakHeartRate) {
        ContentValues values = new ContentValues();

        values.put(Sessions.STARTING_TIME, startingTime);
        values.put(Sessions.ENDING_TIME, endingTime);
        values.put(Sessions.STEPS_TAKEN, stepsTaken);
        values.put(Sessions.ENERGY_BURNT, energyBurnt);
        values.put(Sessions.DISTANCE, distance);
        values.put(Sessions.AVERAGE_SPEED, avgSpeed);
        values.put(Sessions.PEAK_SPEED, peakSpeed);
        values.put(Sessions.AVERAGE_HEART_RATE, avgHeartRate);
        values.put(Sessions.PEAK_HEART_RATE, peakHeartRate);

        return values;
    }
}
