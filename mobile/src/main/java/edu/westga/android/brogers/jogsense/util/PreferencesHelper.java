package edu.westga.android.brogers.jogsense.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.TypedValue;
import android.widget.Toast;

import edu.westga.android.brogers.jogsense.R;

/**
 * PreferencesHelper - Helper class intended to wrap calls to getter/setter methods dealing with
 * application preferences. This will help to reduce code clutter associated with allocating
 * resources needed for the calls, not to mention preserve memory due to the resources' reduced scope.
 *
 * @author Brian Rogers
 * @version October 22nd, 2014
 */
@SuppressWarnings("UnusedDeclaration")
public class PreferencesHelper {
    private Context context;
    private SharedPreferences preferences;
    private SharedPreferences.Editor preferencesEditor;

    /**
     * Creates an instance of the helper class.
     *
     * @param context The interface with which to resolve resources.
     */
    public PreferencesHelper(Context context) {
        this.context = context;
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.preferencesEditor = preferences.edit();
    }

    /**
     * Retrieves a boolean value from the preferences.
     *
     * @param keyReference          The reference to the preference to retrieve.
     * @param defaultValueReference The reference to the default value to retrieve, if the preference
     *                              does not exist.
     * @return The preference value if it exists, or the default value.
     */
    public boolean getBoolean(final int keyReference, final int defaultValueReference) {
        String key = context.getString(keyReference);
        return getBoolean(key, defaultValueReference);
    }

    /**
     * Retrieves a boolean value from the preferences.
     *
     * @param key                   The key for the preference to retrieve.
     * @param defaultValueReference The reference to the default value to retrieve, if the preference
     *                              does not exist.
     * @return The preference value if it exists, or the default value.
     */
    public boolean getBoolean(final String key, final int defaultValueReference) {
        boolean defaultValue = context.getResources().getBoolean(defaultValueReference);
        return preferences.getBoolean(key, defaultValue);
    }

    /**
     * Sets a boolean value in the preferences.
     *
     * @param keyReference The reference to the preference to write to.
     * @param value        The value to be written.
     * @return true if the value was successfully written, false otherwise.
     */
    public boolean setBoolean(final int keyReference, final boolean value) {
        String key = context.getString(keyReference);
        return setBoolean(key, value);
    }

    /**
     * Sets a boolean value in the preferences.
     *
     * @param key   The key for the preference to write to.
     * @param value The value to be written.
     * @return true if the value was successfully written, false otherwise.
     */
    public boolean setBoolean(final String key, final boolean value) {
        preferencesEditor.putBoolean(key, value);
        boolean saved = preferencesEditor.commit();

        if (!saved) {
            String error = context.getString(R.string.error_settings_write_failed);
            Toast.makeText(context, error, Toast.LENGTH_LONG).show();
        }

        return saved;
    }

    /**
     * Retrieves a float value from the preferences.
     *
     * @param keyReference          The reference to the preference to retrieve.
     * @param defaultValueReference The reference to the default value to retrieve, if the preference
     *                              does not exist.
     * @return The preference value if it exists, or the default value.
     */
    public float getFloat(final int keyReference, final int defaultValueReference) {
        String key = context.getString(keyReference);
        return getFloat(key, defaultValueReference);
    }

    /**
     * Retrieves a float value from the preferences.
     *
     * @param key                   The key for the preference to retrieve.
     * @param defaultValueReference The reference to the default value to retrieve, if the preference
     *                              does not exist.
     * @return The preference value if it exists, or the default value.
     */
    public float getFloat(final String key, final int defaultValueReference) {
        TypedValue outValue = new TypedValue();
        context.getResources().getValue(defaultValueReference, outValue, true);
        float defaultValue = outValue.getFloat();

        return preferences.getFloat(key, defaultValue);
    }

    /**
     * Sets a float value in the preferences.
     *
     * @param keyReference The reference to the preference to write to.
     * @param value        The value to be written.
     * @return true if the value was successfully written, false otherwise.
     */
    public boolean setFloat(final int keyReference, final float value) {
        String key = context.getString(keyReference);
        return setFloat(key, value);
    }

    /**
     * Sets a float value in the preferences.
     *
     * @param key   The key for the preference to write to.
     * @param value The value to be written.
     * @return true if the value was successfully written, false otherwise.
     */
    public boolean setFloat(final String key, final float value) {
        preferencesEditor.putFloat(key, value);
        boolean saved = preferencesEditor.commit();

        if (!saved) {
            String error = context.getString(R.string.error_settings_write_failed);
            Toast.makeText(context, error, Toast.LENGTH_LONG).show();
        }

        return saved;
    }

    /**
     * Retrieves an integer value from the preferences.
     *
     * @param keyReference          The reference to the preference to retrieve.
     * @param defaultValueReference The reference to the default value to retrieve, if the preference
     *                              does not exist.
     * @return The preference value if it exists, or the default value.
     */
    public int getInt(final int keyReference, final int defaultValueReference) {
        String key = context.getString(keyReference);
        return getInt(key, defaultValueReference);
    }

    /**
     * Retrieves an integer value from the preferences.
     *
     * @param key                   The key for the preference to retrieve.
     * @param defaultValueReference The reference to the default value to retrieve, if the preference
     *                              does not exist.
     * @return The preference value if it exists, or the default value.
     */
    public int getInt(final String key, final int defaultValueReference) {
        int defaultValue = context.getResources().getInteger(defaultValueReference);
        return preferences.getInt(key, defaultValue);
    }

    /**
     * Sets an integer value in the preferences.
     *
     * @param keyReference The reference to the preference to write to.
     * @param value        The value to be written.
     * @return true if the value was successfully written, false otherwise.
     */
    public boolean setInt(final int keyReference, final int value) {
        String key = context.getString(keyReference);
        return setInt(key, value);
    }

    /**
     * Sets an integer value in the preferences.
     *
     * @param key   The key for the preference to write to.
     * @param value The value to be written.
     * @return true if the value was successfully written, false otherwise.
     */
    public boolean setInt(final String key, final int value) {
        preferencesEditor.putInt(key, value);
        boolean saved = preferencesEditor.commit();

        if (!saved) {
            String error = context.getString(R.string.error_settings_write_failed);
            Toast.makeText(context, error, Toast.LENGTH_LONG).show();
        }

        return saved;
    }

    /**
     * Retrieves a long value from the preferences.
     *
     * @param keyReference          The reference to the preference to retrieve.
     * @param defaultValueReference The reference to the default value to retrieve, if the preference
     *                              does not exist.
     * @return The preference value if it exists, or the default value.
     */
    public long getLong(final int keyReference, final int defaultValueReference) {
        String key = context.getString(keyReference);
        return getLong(key, defaultValueReference);
    }

    /**
     * Retrieves a long value from the preferences.
     *
     * @param key                   The key for the preference to retrieve.
     * @param defaultValueReference The reference to the default value to retrieve, if the preference
     *                              does not exist.
     * @return The preference value if it exists, or the default value.
     */
    public long getLong(final String key, final int defaultValueReference) {
        TypedValue outValue = new TypedValue();
        context.getResources().getValue(defaultValueReference, outValue, true);
        long defaultValue = (long) outValue.getFloat();

        return preferences.getLong(key, defaultValue);
    }

    /**
     * Sets a long value in the preferences.
     *
     * @param keyReference The reference to the preference to write to.
     * @param value        The value to be written.
     * @return true if the value was successfully written, false otherwise.
     */
    public boolean setLong(final int keyReference, final long value) {
        String key = context.getString(keyReference);
        return setLong(key, value);
    }

    /**
     * Sets a long value in the preferences.
     *
     * @param key   The key for the preference to write to.
     * @param value The value to be written.
     * @return true if the value was successfully written, false otherwise.
     */
    public boolean setLong(final String key, final long value) {
        preferencesEditor.putLong(key, value);
        boolean saved = preferencesEditor.commit();

        if (!saved) {
            String error = context.getString(R.string.error_settings_write_failed);
            Toast.makeText(context, error, Toast.LENGTH_LONG).show();
        }

        return saved;
    }

    /**
     * Retrieves a String value from the preferences.
     *
     * @param keyReference          The reference to the preference to retrieve.
     * @param defaultValueReference The reference to the default value to retrieve, if the preference
     *                              does not exist.
     * @return The preference value if it exists, or the default value.
     */
    public String getString(final int keyReference, final int defaultValueReference) {
        String key = context.getString(keyReference);
        return getString(key, defaultValueReference);
    }

    /**
     * Retrieves a String value from the preferences.
     *
     * @param key                   The key for the preference to retrieve.
     * @param defaultValueReference The reference to the default value to retrieve, if the preference
     *                              does not exist.
     * @return The preference value if it exists, or the default value.
     */
    public String getString(final String key, final int defaultValueReference) {
        String defaultValue = context.getString(defaultValueReference);
        return preferences.getString(key, defaultValue);
    }

    /**
     * Sets a String value in the preferences.
     *
     * @param keyReference The reference to the preference to write to.
     * @param value        The value to be written.
     * @return true if the value was successfully written, false otherwise.
     */
    public boolean setString(final int keyReference, final String value) {
        String key = context.getString(keyReference);
        return setString(key, value);
    }

    /**
     * Sets a String value in the preferences.
     *
     * @param key   The key for the preference to write to.
     * @param value The value to be written.
     * @return true if the value was successfully written, false otherwise.
     */
    public boolean setString(final String key, final String value) {
        preferencesEditor.putString(key, value);
        boolean saved = preferencesEditor.commit();

        if (!saved) {
            String error = context.getString(R.string.error_settings_write_failed);
            Toast.makeText(context, error, Toast.LENGTH_LONG).show();
        }

        return saved;
    }
}
