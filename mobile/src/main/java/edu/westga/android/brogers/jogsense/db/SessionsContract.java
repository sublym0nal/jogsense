package edu.westga.android.brogers.jogsense.db;

import android.provider.BaseColumns;

/**
 * Created by Brian Rogers on 11/4/2014.
 */
public final class SessionsContract {
    private SessionsContract() {

    }

    public abstract static class Sessions implements BaseColumns {
        public static final String TABLE_NAME = "sessions";

        /**
         * The unique identifier for this row.
         */
        public static final String ID = "_id";

        /**
         * The timestamp taken at the beginning of the session.
         */
        public static final String STARTING_TIME = "startingTime";

        /**
         * The timestamp taken at the end of the session.
         */
        public static final String ENDING_TIME = "endingTime";

        /**
         * The amount of steps taken in the session.
         */
        public static final String STEPS_TAKEN = "stepsTaken";

        /**
         * The energy burnt in the session (in terms of calories).
         */
        public static final String ENERGY_BURNT = "energyBurnt";

        /**
         * The distance covered in the session (in terms of meters).
         */
        public static final String DISTANCE = "distance";

        /**
         * The average speed measured throughout the session (in terms of meters per second).
         */
        public static final String AVERAGE_SPEED = "avgSpeed";

        /**
         * The peak speed measured throughout the session (in terms of meters per second).
         */
        public static final String PEAK_SPEED = "peakSpeed";

        /**
         * The average heart rate measured throughout the session (in terms of beats per minute).
         */
        public static final String AVERAGE_HEART_RATE = "avgHeartRate";

        /**
         * The peak heart rate measured throughout the session (in terms of beats per minute).
         */
        public static final String PEAK_HEART_RATE = "peakHeartRate";

        public static final String[] ALL_COLUMNS = {ID, STARTING_TIME, ENDING_TIME, STEPS_TAKEN,
                ENERGY_BURNT, DISTANCE, AVERAGE_SPEED, PEAK_SPEED, AVERAGE_HEART_RATE,
                PEAK_HEART_RATE};

        private Sessions() {

        }
    }

    public abstract static class Coordinates implements BaseColumns {
        public static final String TABLE_NAME = "coordinates";

        /**
         * The unique identifier for this row.
         */
        public static final String ID = "_id";

        /**
         * The session (with ID as foreign key) this coordinate was measured in.
         */
        public static final String SESSION_ID = "sessionId";

        /**
         * The order in which this coordinate was measured (as a zero-based index).
         */
        public static final String SEQUENCE_ID = "sequenceId";

        /**
         * The latitude measurement made by the Google Maps API.
         */
        public static final String LATITUDE = "latitude";

        /**
         * The longitude measurement made by the Google Maps API.
         */
        public static final String LONGITUDE = "longitude";

        public static final String[] ALL_COLUMNS = {ID, SESSION_ID, SEQUENCE_ID, LATITUDE,
                LONGITUDE};

        private Coordinates() {

        }
    }
}
