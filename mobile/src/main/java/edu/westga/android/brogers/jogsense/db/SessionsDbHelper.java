package edu.westga.android.brogers.jogsense.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static edu.westga.android.brogers.jogsense.db.SessionsContract.Coordinates;
import static edu.westga.android.brogers.jogsense.db.SessionsContract.Sessions;

/**
 * Created by Brian Rogers on 11/4/2014.
 */
public class SessionsDbHelper extends SQLiteOpenHelper {
    public static final String ORDER_BY_DESCENDING = " DESC";

    private static final String DATABASE_NAME = "sessions.db";
    private static final int DATABASE_VERSION = 1;

    private static final String CREATE_TABLE_COMMAND = "CREATE TABLE ";

    private static final String PRIMARY_KEY_WITH_AUTOINCREMENT_CONSTRAINT = " PRIMARY KEY AUTOINCREMENT";
    private static final String UNIQUE_CONSTRAINT = " UNIQUE";
    private static final String NOT_NULL_CONSTRAINT = " NOT NULL";

    private static final String INTEGER_TYPE = " INTEGER";
    private static final String REAL_TYPE = " REAL";

    private static final String FOREIGN_KEY_REFERENCES_CLAUSE = " REFERENCES ";
    private static final String FOREIGN_KEY_ON_UPDATE_REACTION = " ON UPDATE";
    private static final String FOREIGN_KEY_ON_DELETE_REACTION = " ON DELETE";
    private static final String CASCADE_ACTION = " CASCADE";

    private static final String COMMA_SEPARATOR = ", ";

    private static final String SQL_CREATE_SESSIONS_TABLE = CREATE_TABLE_COMMAND + Sessions.TABLE_NAME + " ("
            + Sessions.ID + INTEGER_TYPE + PRIMARY_KEY_WITH_AUTOINCREMENT_CONSTRAINT + COMMA_SEPARATOR
            + Sessions.STARTING_TIME + REAL_TYPE + NOT_NULL_CONSTRAINT + UNIQUE_CONSTRAINT + COMMA_SEPARATOR
            + Sessions.ENDING_TIME + REAL_TYPE + NOT_NULL_CONSTRAINT + UNIQUE_CONSTRAINT + COMMA_SEPARATOR
            + Sessions.STEPS_TAKEN + REAL_TYPE + COMMA_SEPARATOR
            + Sessions.ENERGY_BURNT + REAL_TYPE + COMMA_SEPARATOR
            + Sessions.DISTANCE + REAL_TYPE + COMMA_SEPARATOR
            + Sessions.AVERAGE_SPEED + REAL_TYPE + COMMA_SEPARATOR
            + Sessions.PEAK_SPEED + REAL_TYPE + COMMA_SEPARATOR
            + Sessions.AVERAGE_HEART_RATE + REAL_TYPE + COMMA_SEPARATOR
            + Sessions.PEAK_HEART_RATE + INTEGER_TYPE
            + ");";

    private static final String SQL_CREATE_COORDINATES_TABLE = CREATE_TABLE_COMMAND + Coordinates.TABLE_NAME + " ("
            + Coordinates.ID + INTEGER_TYPE + PRIMARY_KEY_WITH_AUTOINCREMENT_CONSTRAINT + COMMA_SEPARATOR
            + Coordinates.SESSION_ID + FOREIGN_KEY_REFERENCES_CLAUSE + Sessions.TABLE_NAME + " ( " +
            Sessions.ID + " )" + FOREIGN_KEY_ON_DELETE_REACTION + CASCADE_ACTION +
            FOREIGN_KEY_ON_UPDATE_REACTION + CASCADE_ACTION + COMMA_SEPARATOR
            + Coordinates.SEQUENCE_ID + INTEGER_TYPE + NOT_NULL_CONSTRAINT + COMMA_SEPARATOR
            + Coordinates.LATITUDE + REAL_TYPE + NOT_NULL_CONSTRAINT + COMMA_SEPARATOR
            + Coordinates.LONGITUDE + REAL_TYPE + NOT_NULL_CONSTRAINT
            + ");";

    public SessionsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(SQL_CREATE_SESSIONS_TABLE);
        database.execSQL(SQL_CREATE_COORDINATES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        // Not implemented.
    }
}
