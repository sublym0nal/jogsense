package edu.westga.android.brogers.jogsense.util;

import android.content.Context;
import android.hardware.SensorManager;
import android.provider.Settings;

import edu.westga.android.brogers.jogsense.R;
import edu.westga.android.brogers.jogsense.shared.SensorDetection;

/**
 * HardwareDetection - Helper class designed to facilitate checking for hardware relevant to the
 * application.
 *
 * @author Brian Rogers
 * @version October 22nd, 2014
 */
public class HardwareDetection {
    private static final String GPS_PROVIDER_KEY = "gps";
    private static final String NETWORK_PROVIDER_KEY = "network";

    private Context context;
    private PreferencesHelper prefHelper;
    private SensorManager sensorManager;

    /**
     * Creates a new HardwareDetection session, and runs a check.
     *
     * @param context The interface with which to resolve resources.
     */
    public HardwareDetection(Context context) {
        this.context = context;
        this.prefHelper = new PreferencesHelper(context);
        this.sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);

        run();
    }

    private static boolean isBothLocationSensorsEnabled(Context context) {
        // Required for support prior to KitKat
        //noinspection deprecation
        String provider = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        return provider.contains(GPS_PROVIDER_KEY) && provider.contains(NETWORK_PROVIDER_KEY);
    }

    /**
     * Checks whether the device's Location service is disabled.
     *
     * @param context The interface with which to resolve resources.
     * @return true if the Location service is disabled, false otherwise.
     */
    public static boolean isLocationServiceDisabled(Context context) {
        // Required for support prior to KitKat
        //noinspection deprecation
        String provider = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        return !(provider.contains(GPS_PROVIDER_KEY) || provider.contains(NETWORK_PROVIDER_KEY));
    }

    private void run() {
        //checkForStepCounter();
        checkForGps();
        //checkForHeartRateSensor();
    }

    private void checkForStepCounter() {
        int settingKeyReference = R.string.step_counter_sensor_key;
        String settingKey = context.getString(settingKeyReference);
        final boolean isStepCounterSettingEnabled = prefHelper.getBoolean(settingKey, R.bool.default_step_counter_sensor);

        if (isStepCounterSettingEnabled) {
            int warningKeyReference = R.string.step_counter_warning_key;
            final boolean isStepCounterWarningEnabled = prefHelper.getBoolean(warningKeyReference, R.bool.default_step_counter_warning);

            if (SensorDetection.hasStepCounterSensor(sensorManager)) {
                return;
            } else if (isStepCounterWarningEnabled) {
                ReminderDialog.showWearableSensorWarningDialog(context, R.string.step_counter,
                        settingKeyReference, warningKeyReference);
            }
        }
    }

    private void checkForGps() {
        final boolean isGpsSettingEnabled = prefHelper.getBoolean(R.string.gps_key, R.bool.default_gps);

        if (isGpsSettingEnabled) {
            final boolean isLocationServicesWarningEnabled = prefHelper.getBoolean(R.string.location_services_warning_key, R.bool.default_location_services_warning);
            final boolean isLocationSourcesWarningEnabled = prefHelper.getBoolean(R.string.location_sources_info_key, R.bool.default_location_sources_info);

            if (isBothLocationSensorsEnabled(context)) {
                return;
            } else if (!isBothLocationSensorsEnabled(context) && !isLocationServiceDisabled(context)
                    && isLocationSourcesWarningEnabled) {
                ReminderDialog.showLocationSourcesInfoDialog(context);
            } else if (isLocationServicesWarningEnabled) {
                ReminderDialog.showLocationServicesWarningDialog(context);
            }
        }
    }

    private void checkForHeartRateSensor() {
        int settingKeyReference = R.string.heart_rate_sensor_key;
        String settingKey = context.getString(settingKeyReference);
        final boolean isHeartRateSensorSettingEnabled = prefHelper.getBoolean(settingKey, R.bool.default_heart_rate_sensor);

        if (isHeartRateSensorSettingEnabled) {
            int warningKeyReference = R.string.heart_rate_sensor_warning_key;
            final boolean isHeartRateSensorWarningEnabled = prefHelper.getBoolean(warningKeyReference, R.bool.default_heart_rate_sensor_warning);

            if (SensorDetection.hasHeartRateSensor(sensorManager)) {
                return;
            } else if (isHeartRateSensorWarningEnabled) {
                ReminderDialog.showWearableSensorWarningDialog(context, R.string.heart_rate_sensor,
                        settingKeyReference, warningKeyReference);
            }
        }
    }
}
