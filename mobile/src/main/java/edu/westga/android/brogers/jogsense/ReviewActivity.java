package edu.westga.android.brogers.jogsense;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;


public class ReviewActivity extends ActionBarActivity {
    public static final String COORDINATES_KEY = "coordinates";
    public static final String SESSION_DURATION_KEY = "sessionDuration";
    public static final String STEPS_TAKEN_KEY = "stepsTaken";
    public static final String ENERGY_BURNT_KEY = "energyBurnt";
    public static final String DISTANCE_KEY = "distance";
    public static final String AVERAGE_SPEED_KEY = "avgSpeed";
    public static final String PEAK_SPEED_KEY = "peakSpeed";
    public static final String AVERAGE_HEART_RATE_KEY = "avgHeartRate";
    public static final String PEAK_HEART_RATE_KEY = "peakHeartRate";
    private static final String TAG = "mobile-ReviewActivity";
    private static final LocationRequest REQUEST_PARAMETERS = LocationRequest.create()
            .setInterval(3000)  // Updates, at minimum, once every 3 seconds
            .setFastestInterval((1 / 60l) * 1000)    // Updates, at maximum, at a rate corresponding to 60 FPS (~16.667 ms)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    private ReviewFragment reviewFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        if (savedInstanceState == null) {
            reviewFragment = new ReviewFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, reviewFragment)
                    .commit();
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public class ReviewFragment extends Fragment implements GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener {
        private GoogleMap map;
        private LocationClient locationClient;

        private ArrayList<LatLng> coordinates;
        private TextView sessionDurationView;
        private TextView stepsTakenView;
        private TextView energyBurntView;
        private TextView distanceView;
        private TextView averageSpeedView;
        private TextView peakSpeedView;
        private TextView avgHeartRateView;
        private TextView peakHeartRateView;

        public ReviewFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_review, container, false);

            getGoogleMap();
            sessionDurationView = (TextView) getView().findViewById(R.id.durationValue);
            stepsTakenView = (TextView) getView().findViewById(R.id.stepsTakenValue);
            energyBurntView = (TextView) getView().findViewById(R.id.energyBurntValue);
            distanceView = (TextView) getView().findViewById(R.id.distanceValue);
            averageSpeedView = (TextView) getView().findViewById(R.id.avgSpeedValue);
            peakSpeedView = (TextView) getView().findViewById(R.id.peakSpeedValue);
            avgHeartRateView = (TextView) getView().findViewById(R.id.avgHeartRateValue);
            peakHeartRateView = (TextView) getView().findViewById(R.id.peakHeartRateValue);

            coordinates = savedInstanceState.getParcelableArrayList(COORDINATES_KEY);

            return rootView;
        }

        private void getGoogleMap() {
            if (map == null) {
                //cmmap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            }

            if (map != null) {
                map.setMyLocationEnabled(true);
                map.getUiSettings().setZoomControlsEnabled(false);
                map.getUiSettings().setZoomGesturesEnabled(false);
            }
        }

        private void getLocationClient() {
            if (locationClient == null) {
                locationClient = new LocationClient(getApplicationContext(),
                        this,   // ConnectionCallbacks
                        this    // OnConnectionFailedListener
                );
            }
        }

        @Override
        public void onConnected(Bundle bundle) {
            if (locationClient.isConnected()) {
                //locationClient.requestLocationUpdates(REQUEST_PARAMETERS, this);
            }
        }

        @Override
        public void onDisconnected() {

        }

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {

        }
    }
}
