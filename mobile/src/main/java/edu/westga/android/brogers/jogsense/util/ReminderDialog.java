package edu.westga.android.brogers.jogsense.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.os.Build;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;

import edu.westga.android.brogers.jogsense.R;
import edu.westga.android.brogers.jogsense.shared.Toaster;

/**
 * ReminderDialog - Helper class containing the logic behind the custom "reminder dialogs"
 * used throughout the application. It allows for changing of settings on the fly in a static
 * context, as well as for permanently dismissing said dialogs.
 *
 * @author Brian Rogers
 * @version October 22nd, 2014
 */
@SuppressWarnings("UnusedDeclaration")
public class ReminderDialog {
    /**
     * Shows a permanently dismissible {@link AlertDialog} warning the user that the Location
     * service needs to be enabled in order for GPS readings to function. The user is given the
     * option to enable the Location service in their device settings, disable GPS readings, or
     * ignore the warning.
     *
     * @param context The interface with which to resolve resources.
     */
    public static void showLocationServicesWarningDialog(final Context context) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View doNotShowAgainView = layoutInflater.inflate(R.layout.do_not_show_again_checkbox, null);
        final CheckBox doNotShowAgainCheckBox = (CheckBox) doNotShowAgainView.findViewById(R.id.checkbox);

        dialog.setView(doNotShowAgainView);
        dialog.setIcon(android.R.drawable.ic_dialog_alert);
        dialog.setTitle(R.string.title_location_services_disabled);
        dialog.setMessage(R.string.location_services_disabled);

        final String reminderKey = context.getString(R.string.location_services_warning_key);

        dialog.setPositiveButton(R.string.settings, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkForPermanentDismissal(context, doNotShowAgainCheckBox, reminderKey);

                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
            }
        });
        dialog.setNeutralButton(R.string.disable_tracking, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkForPermanentDismissal(context, doNotShowAgainCheckBox, reminderKey);

                PreferencesHelper prefHelper = new PreferencesHelper(context);

                if (prefHelper.setBoolean(R.string.gps_key, false)) {
                    String hardwareName = context.getString(R.string.gps);
                    Toaster.showShortToast(context, R.string.hardware_tracking_disabled, hardwareName);
                }
            }
        });
        dialog.setNegativeButton(R.string.ignore, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkForPermanentDismissal(context, doNotShowAgainCheckBox, reminderKey);
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    checkForPermanentDismissal(context, doNotShowAgainCheckBox, reminderKey);
                }
            });
        }

        dialog.show();
    }

    /**
     * Shows a permanently dismissible informational {@link AlertDialog} informing the user that
     * they can increase the accuracy of their Location readings by changing their device's
     * settings. The user is given the option to either make changes to their device's Location
     * settings or ignore the tip.
     *
     * @param context The interface with which to resolve resources.
     */
    public static void showLocationSourcesInfoDialog(final Context context) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View doNotShowAgainView = layoutInflater.inflate(R.layout.do_not_show_again_checkbox, null);
        final CheckBox doNotShowAgainCheckBox = (CheckBox) doNotShowAgainView.findViewById(R.id.checkbox);

        dialog.setView(doNotShowAgainView);
        dialog.setIcon(android.R.drawable.ic_dialog_info);
        dialog.setTitle(R.string.title_gps_accuracy_tip);

        /* KitKat made some changes to the way Location services are toggled by the user. A
           different message is shown to accommodate these changes. */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            dialog.setMessage(R.string.gps_accuracy_tip_high_accuracy);
        } else {
            dialog.setMessage(R.string.gps_accuracy_tip_both_location_sources);
        }

        String settings = context.getString(R.string.settings);
        String ignore = context.getString(R.string.ignore);
        final String reminderKey = context.getString(R.string.location_sources_info_key);

        dialog.setPositiveButton(settings, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkForPermanentDismissal(context, doNotShowAgainCheckBox, reminderKey);

                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
            }
        });
        dialog.setNegativeButton(ignore, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkForPermanentDismissal(context, doNotShowAgainCheckBox, reminderKey);
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    checkForPermanentDismissal(context, doNotShowAgainCheckBox, reminderKey);
                }
            });
        }

        dialog.show();
    }

    /**
     * Shows a permanently dismissible {@link AlertDialog} warning the user that an Android Wear
     * {@link Sensor} was not detected, and that. The user is given the option to check their Android Wear
     * settings, disable the related preference, or ignore the warning.
     *
     * @param context               The interface with which to resolve resources.
     * @param hardwareNameReference The reference to the name of the hardware.
     * @param settingKeyReference   The reference to the key associated with the listening preference.
     * @param warningKeyReference   The reference to the key associated with the warning preference.
     */
    public static void showWearableSensorWarningDialog(final Context context, final int hardwareNameReference,
                                                       final int settingKeyReference, final int warningKeyReference) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View doNotShowAgainView = layoutInflater.inflate(R.layout.do_not_show_again_checkbox, null);
        final CheckBox doNotShowAgainCheckBox = (CheckBox) doNotShowAgainView.findViewById(R.id.checkbox);

        dialog.setView(doNotShowAgainView);
        dialog.setIcon(android.R.drawable.ic_dialog_alert);

        final String hardwareName = context.getString(hardwareNameReference);
        String title = context.getString(R.string.title_wear_sensor_not_detected, hardwareName);
        String message = context.getString(R.string.wear_sensor_not_detected, hardwareName, hardwareName);

        dialog.setTitle(title);
        dialog.setMessage(message);

        final String reminderKey = context.getString(warningKeyReference);

        dialog.setPositiveButton(R.string.settings, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkForPermanentDismissal(context, doNotShowAgainCheckBox, reminderKey);

                PackageManager packageManager = context.getPackageManager();
                String packageName = context.getString(R.string.android_wear_package_name);
                Intent intent = packageManager.getLaunchIntentForPackage(packageName);

                if (intent == null) {
                    Toaster.showLongToast(context, R.string.error_android_wear_not_found);
                } else {
                    intent.addCategory(Intent.CATEGORY_LAUNCHER);
                    context.startActivity(intent);
                }
            }
        });
        dialog.setNeutralButton(R.string.disable_tracking, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkForPermanentDismissal(context, doNotShowAgainCheckBox, reminderKey);

                PreferencesHelper prefHelper = new PreferencesHelper(context);

                if (prefHelper.setBoolean(settingKeyReference, false)) {
                    Toaster.showShortToast(context, R.string.hardware_tracking_disabled, hardwareName);
                }
            }
        });
        dialog.setNegativeButton(R.string.ignore, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkForPermanentDismissal(context, doNotShowAgainCheckBox, reminderKey);
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    checkForPermanentDismissal(context, doNotShowAgainCheckBox, reminderKey);
                }
            });
        }

        dialog.show();
    }

    private static void checkForPermanentDismissal(Context context, final CheckBox checkBox, final String reminderKey) {
        if (checkBox.isChecked()) {
            PreferencesHelper prefHelper = new PreferencesHelper(context);

            if (prefHelper.setBoolean(reminderKey, false)) {
                Toaster.showShortToast(context, R.string.reminder_permanently_dismissed);
            }
        }
    }
}
