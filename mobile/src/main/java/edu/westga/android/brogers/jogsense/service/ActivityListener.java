package edu.westga.android.brogers.jogsense.service;

import android.content.Intent;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import edu.westga.android.brogers.jogsense.SessionListActivity;
import edu.westga.android.brogers.jogsense.SettingsActivity;

import static edu.westga.android.brogers.jogsense.shared.DataCommunication.MessageType;

/**
 * Created by Brian Rogers on 11/5/2014.
 */
public class ActivityListener extends WearableListenerService {
    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        String messageType = messageEvent.getPath();

        if (messageType.equals(MessageType.REVIEW_SESSIONS.toString())) {
            Intent intent = new Intent(this, SessionListActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (messageType.equals(MessageType.CONFIGURE_SETTINGS.toString())) {
            Intent intent = new Intent(this, SettingsActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
}
