package edu.westga.android.brogers.jogsense;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.nio.ByteBuffer;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import edu.westga.android.brogers.jogsense.shared.SensorDetection;
import edu.westga.android.brogers.jogsense.shared.Toaster;

import static edu.westga.android.brogers.jogsense.shared.DataCommunication.CONNECTION_TIMEOUT_IN_MILLISECONDS;
import static edu.westga.android.brogers.jogsense.shared.DataCommunication.MessageType;
import static edu.westga.android.brogers.jogsense.shared.DataCommunication.buildGoogleWearableApiClient;

public class MainActivity extends Activity implements SensorEventListener {
    private static final String TAG = "Wear-MainActivity";

    private static final int STEP_COUNTER_SENSOR_PRIMING_DELAY = 10;
    private CountDownLatch stepCounterCountDown = new CountDownLatch(STEP_COUNTER_SENSOR_PRIMING_DELAY);
    private static final int HEART_RATE_SENSOR_PRIMING_DELAY = 30;
    private CountDownLatch heartRateCountDown = new CountDownLatch(HEART_RATE_SENSOR_PRIMING_DELAY);

    private TextView heartRateValueLabel;
    private TextView stepsTakenValueLabel;
    private GoogleApiClient googleApiClient;
    private SensorManager sensorManager;
    private Sensor stepCounterSensor;
    private Sensor heartRateSensor;
    private int initialStepCount = -1;
    private String nodeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                heartRateValueLabel = (TextView) stub.findViewById(R.id.heartRateValue);
                stepsTakenValueLabel = (TextView) stub.findViewById(R.id.stepsTakenValue);
            }
        });

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        int stepSensorId = SensorDetection.getIdealStepCounterSensor(sensorManager);
        int heartRateSensorId = SensorDetection.getIdealHeartRateSensor(sensorManager);

        stepCounterSensor = sensorManager.getDefaultSensor(stepSensorId);
        heartRateSensor = sensorManager.getDefaultSensor(heartRateSensorId);

        googleApiClient = buildGoogleWearableApiClient(this);
        getPairedWearNodeId(googleApiClient);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onStart() {
        super.onStart();

        registerStepCounterSensor();
        registerHeartRateSensor();

        Toaster.showLongToast(this, R.string.user_prompt_wait_for_values);
    }

    private void registerStepCounterSensor() {
        boolean registrationSuccessful = sensorManager.registerListener(this, stepCounterSensor,
                SensorManager.SENSOR_STATUS_ACCURACY_HIGH);

        if (registrationSuccessful) {
            Log.d(TAG, "Listener registered for step counter sensor.");

            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (stepCounterCountDown.getCount() != 0) {
                        try {
                            Thread.sleep(1000);
                            stepCounterCountDown.countDown();
                            Log.d(TAG, "Seconds until step sensor is primed: " + stepCounterCountDown.getCount());
                        } catch (InterruptedException ex) {
                            Log.wtf(TAG, ex);
                        }
                    }

                    Log.d(TAG, "Starting step counter sensor readings...");
                }
            }).start();
        } else {
            Log.e(TAG, "Error registering listener for step counter sensor!");
        }
    }

    private void registerHeartRateSensor() {
        boolean registrationSuccessful = sensorManager.registerListener(this, heartRateSensor,
                SensorManager.SENSOR_STATUS_ACCURACY_HIGH);

        if (registrationSuccessful) {
            Log.d(TAG, "Listener registered for heart rate sensor.");

            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (heartRateCountDown.getCount() != 0) {
                        try {
                            Thread.sleep(1000);
                            heartRateCountDown.countDown();
                            Log.d(TAG, "Seconds until heart rate sensor is primed: " + heartRateCountDown.getCount());
                        } catch (InterruptedException ex) {
                            Log.wtf(TAG, ex);
                        }
                    }

                    Log.d(TAG, "Starting heart rate sensor readings...");
                }
            }).start();
        } else {
            Log.e(TAG, "Error registering listener for heart rate sensor!");
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Sensor callingSensor = event.sensor;

        if (SensorDetection.isStepCounterSensor(callingSensor)) {
            Log.d(TAG, "Step Counter Sensor fired.");
            sendStepCounterData(event);
        } else if (SensorDetection.isHeartRateSensor(callingSensor)) {
            Log.d(TAG, "Heart Rate Sensor fired.");
            sendHeartRateData(event);
        }
    }

    private void sendStepCounterData(SensorEvent event) {
        int stepCount = (int) event.values[0];
        Log.d(TAG, "Raw step count value: " + stepCount);

        if (initialStepCount == -1) {
            initialStepCount = stepCount - 1;
            Log.d(TAG, "Initial step count set to: " + initialStepCount);
        }

        int sessionStepCount = stepCount - initialStepCount;
        Log.d(TAG, "Steps taken so far:" + sessionStepCount);

        if (stepsTakenValueLabel != null) {
            Locale userLocale = getResources().getConfiguration().locale;
            NumberFormat formatter = NumberFormat.getNumberInstance(userLocale);
            String formattedSessionStepCount = formatter.format(sessionStepCount);

            stepsTakenValueLabel.setText(formattedSessionStepCount);
        }

        ByteBuffer buffer = ByteBuffer.allocate(4);
        buffer.putInt(sessionStepCount);

        sendMessage(googleApiClient, nodeId, MessageType.STEP_COUNTER, buffer.array());
    }

    private void sendHeartRateData(SensorEvent event) {
        try {
            heartRateCountDown.await();

            if (event.accuracy == SensorManager.SENSOR_STATUS_NO_CONTACT ||
                    event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
                Log.e(TAG, "Read failed!");
                return;
            }

            int reading = (int) event.values[0];

            if (reading > 0) {
                Log.i(TAG, "Heart Rate: " + reading + " BPM");
                heartRateValueLabel.setText(getString(R.string.beats_per_minute_format, reading));

                ByteBuffer buffer = ByteBuffer.allocate(4);
                buffer.putInt(reading);

                sendMessage(googleApiClient, nodeId, MessageType.HEART_RATE, buffer.array());
            } else {
                Log.i(TAG, "Invalid reading; discarding.");
            }
        } catch (InterruptedException ex) {
            Log.wtf(TAG, ex);
        }
    }

    /**
     * Gets the ID associated with the device paired via Android Wear.
     *
     * @param googleApiClient The main entry point for Google Play Services integration.
     * @return The ID of the first Android Wear {@link com.google.android.gms.wearable.Node} found, or null if no nodes were found.
     * @pre Only one handheld device is paired to the Wear device, and vice versa.
     */
    public void getPairedWearNodeId(final GoogleApiClient googleApiClient) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
                googleApiClient.blockingConnect(CONNECTION_TIMEOUT_IN_MILLISECONDS, TimeUnit.MILLISECONDS);
                Log.d(TAG, "GoogleApiClient connected.");

                NodeApi.GetConnectedNodesResult result = Wearable.NodeApi.getConnectedNodes(googleApiClient).await();
                List<Node> nodes = result.getNodes();
                Log.d(TAG, "List of connected Nodes retrieved.");

                if (!nodes.isEmpty()) {
                    for (Node node : nodes) {
                        Log.d(TAG, "Node found: " + node.getDisplayName() + " (" + node.getId() + ")");
                    }

                    nodeId = nodes.get(0).getId();
                    Log.i(TAG, "Selected Node with ID (" + nodeId + ")");

                    return;
                } else {
                    Log.e(TAG, "No Nodes found!");
                }

                googleApiClient.disconnect();
                Log.d(TAG, "GoogleApiClient disconnected.");
            }
        }).start();
    }

    /**
     * Attempts to send a Message to a given {@link Node}.
     *
     * @param googleApiClient The main entry point for Google Play Services integration.
     * @param nodeId          The ID of a {@link Node} on the Android Wear network.
     * @param messageType     The type of Message that the receiving {@link Node} should interpret.
     * @param data            Raw data to be sent to the target {@link Node}, if any.
     */
    public void sendMessage(final GoogleApiClient googleApiClient, final String nodeId,
                            final MessageType messageType, final byte[] data) {
        Log.d(TAG, "Sending Message...");
        new Thread(new Runnable() {
            @Override
            public void run() {
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
                googleApiClient.blockingConnect(CONNECTION_TIMEOUT_IN_MILLISECONDS, TimeUnit.MILLISECONDS);
                Log.d(TAG, "GoogleApiClient connected.");

                MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(googleApiClient,
                        nodeId, messageType.toString(), data).await();

                if (result.getStatus().isSuccess()) {
                    Log.d(TAG, "Message successfully sent!");
                } else {
                    Log.e(TAG, "Failed to send Message: " + result.getStatus());
                }

                googleApiClient.disconnect();
                Log.d(TAG, "GoogleApiClient disconnected.");
            }
        }).start();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Not used in this implementation.
    }

    @Override
    protected void onStop() {
        super.onStop();

        sensorManager.unregisterListener(this);
        Log.d(TAG, "All listeners unregistered.");
    }
}
