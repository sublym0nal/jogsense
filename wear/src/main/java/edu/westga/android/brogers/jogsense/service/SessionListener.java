package edu.westga.android.brogers.jogsense.service;

import android.content.Intent;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import edu.westga.android.brogers.jogsense.MainActivity;

import static edu.westga.android.brogers.jogsense.shared.DataCommunication.MessageType;

/**
 * Created by Brian Rogers on 10/29/2014.
 */
public class SessionListener extends WearableListenerService {
    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        String messageType = messageEvent.getPath();

        if (messageType.equals(MessageType.SESSION_START.toString())) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
}
